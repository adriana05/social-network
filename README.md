This repository was created for connecting people through a basic social network.
The project is completely using JavaFX and Java.

The project was built on a Database repository for data manage.

The "Log In/Sign in" interface for the Social Network. 

![Alt text](https://bitbucket.org/adriana05/social-network/raw/4e49659a7a11d7d627a2d06b359fdc81835d939e/src/main/resources/images/loginApp.PNG)

![Alt text](https://bitbucket.org/adriana05/social-network/raw/4e49659a7a11d7d627a2d06b359fdc81835d939e/src/main/resources/images/signInApp.PNG)


The Profile interface for the user logged in contains the next features:
	-List of friends
	-List of received emails/messages(clicked on, they open the chat giving the possibility to Reply the user who sent it/Reply to all users who have received this email)
	-Remove (clicked on, the button deletes the friend selected from the friend list)
	-Chat (select one user and this will open the chat where you can see the conversation/ select one email and this will open the chat where you can easily reply)
	-Find friends (opens a new pageDTO where you can search through the Network)
	
Opening the profile of a user:

![Alt text](https://bitbucket.org/adriana05/social-network/raw/a24534042e0c03b832e7bbee87d42c137caff831/src/main/resources/images/poz3.PNG)
	
The conversation between you and an user: 

![Alt text](https://bitbucket.org/adriana05/social-network/raw/a24534042e0c03b832e7bbee87d42c137caff831/src/main/resources/images/po4.PNG)


The group chat :

![Alt text](https://bitbucket.org/adriana05/social-network/raw/a24534042e0c03b832e7bbee87d42c137caff831/src/main/resources/images/poz6.PNG)	

The pageDTO for searching through the Network include the features:
	-Search for an user that is not in your friend list and with a click you can see his/her profile(including mutual friends)
	-Send a Friend Request
	-Cancel a Friend Request
	-Answer a Friend Request


![Alt text](https://bitbucket.org/adriana05/social-network/raw/a24534042e0c03b832e7bbee87d42c137caff831/src/main/resources/images/poz7.PNG)

![Alt text](https://bitbucket.org/adriana05/social-network/raw/a24534042e0c03b832e7bbee87d42c137caff831/src/main/resources/images/poz8.PNG)


Events (creating, subscribing, use the feature REMIND ME if you want to get notifications or not when the date of the event is closer):

![Alt text](https://bitbucket.org/adriana05/social-network/raw/a24534042e0c03b832e7bbee87d42c137caff831/src/main/resources/images/poz5.PNG)

Activity Log of the recent activity concerning friendships created and messages with a specific friend/ all messages:

![Alt text](https://bitbucket.org/adriana05/social-network/raw/a24534042e0c03b832e7bbee87d42c137caff831/src/main/resources/images/poz9.PNG)

![Alt text](https://bitbucket.org/adriana05/social-network/raw/1a6854e7fdc7226f38de865a566ec8c5985ab039/src/main/resources/images/poz10.PNG)
