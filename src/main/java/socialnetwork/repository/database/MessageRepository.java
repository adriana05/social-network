package socialnetwork.repository.database;

import socialnetwork.domain.Message;
import socialnetwork.domain.User;
import socialnetwork.repository.exceptions.RepositoryExceptions;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class MessageRepository implements PagingRepository<Long, Message> {
    private final String url;
    private final String username;
    private final String password;
    Connection connection;
    PreparedStatement statement;

    public MessageRepository(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;

        getConnection();
    }

    protected void getConnection() {
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not connect to the DB\n");
        }
    }

    public Message findOne(Long id) {
        if (id == null) {
            throw new RepositoryExceptions("ID must not be null!\n");
        }

        try {
            statement = connection.prepareStatement("SELECT mid, text, from_id, omid, date_message from message where mid =?");
            statement.setLong(1, id);

            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                return null;
            }

            String text = resultSet.getString(2);
            Long from = resultSet.getLong(3);
            Long isReply = resultSet.getLong(4);
            LocalDateTime date = resultSet.getTimestamp(5).toLocalDateTime();

            User sender = findUser(from);

            if (sender == null) {
                throw new RepositoryExceptions("User not found!\n");
            }

            List<User> list = findToUsers(id);

            Message message = new Message(sender, list, text);
            message.setId(id);
            message.setIsReply(isReply);
            message.setDate(date);
            return message;

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query!\n");
        }
    }


    public Iterable<Message> findAll() {
        List<Message> list = new ArrayList<>();

        try {
            statement = connection.prepareStatement("SELECT * from message");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long mid = resultSet.getLong(1);
                String text = resultSet.getString(2);
                Long from = resultSet.getLong(3);
                Long isReply = resultSet.getLong(4);
                LocalDateTime date = resultSet.getTimestamp(5).toLocalDateTime();

                User user = findUser(from);
                List<User> listUsers = findToUsers(mid);

                Message message = new Message(user, listUsers, text);

                message.setId(mid);
                message.setIsReply(isReply);
                message.setDate(date);
                list.add(message);
            }

            return list;

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query!\n");
        }
    }

    public Message save(Message entity) {
        if (entity == null) {
            throw new RepositoryExceptions("Entity cannot be null!\n");
        }

        try {
            statement = connection.prepareStatement("INSERT INTO message (mid, text, from_id, omid, date_message) values (?,?,?,?,?)");
            statement.setLong(1, entity.getId());
            statement.setString(2, entity.getMessage());
            statement.setLong(3, entity.getFrom().getId());
            if (entity.getIsReply() == null)
                statement.setNull(4, Types.BIGINT);
            else
                statement.setLong(4, entity.getIsReply());
            statement.setTimestamp(5, Timestamp.valueOf(entity.getDate()));
            statement.executeUpdate();

            addToRecipients(entity);

            return entity;
        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query!\n");
        }

    }

    public Message delete(Long id) {
        if (id == null) {
            throw new RepositoryExceptions("ID could not be null!\n");
        }

        Message message = findOne(id);
        if (message == null) {
            throw new RepositoryExceptions("Could not find the message!\n");
        }

        try {
            statement = connection.prepareStatement("DELETE from message where mid = ?");
            statement.setLong(1, id);
            statement.executeUpdate();

            return message;

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query!\n");
        }
    }

    public Message update(Message entity) {
        if (entity == null) {
            throw new RepositoryExceptions("Entity could not be null!\n");
        }

        Message message = findOne(entity.getId());

        try {
            statement = connection.prepareStatement("UPDATE message where mid = ? set text =? , from_id = ?, omid =?, date_message = ? ");
            statement.setLong(1, entity.getId());
            statement.setString(2, entity.getMessage());
            statement.setLong(3, entity.getFrom().getId());
            if (entity.getIsReply() == null)
                statement.setNull(4, Types.BIGINT);
            else
                statement.setLong(4, entity.getIsReply());
            statement.setTimestamp(5, Timestamp.valueOf(entity.getDate()));

            statement.executeUpdate();
            return message;

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query!\n");
        }
    }


    private void addToRecipients(Message entity) {

        entity.getTo().forEach(x -> {
            try {
                statement = connection.prepareStatement("INSERT into recipients(uid,mid) values (?,?)");
                statement.setLong(1, x.getId());
                statement.setLong(2, entity.getId());

                statement.executeUpdate();
            } catch (SQLException sqlException) {
                throw new RepositoryExceptions("Could not execute the Query!\n");
            }

        });
    }


    private List<User> findToUsers(Long id) {
        List<User> list = new ArrayList<>();

        try {
            statement = connection.prepareStatement("SELECT uid from recipients where mid = ? ");
            statement.setLong(1, id);

            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long idUser = resultSet.getLong(1);
                User user = findUser(idUser);
                if (user != null) {
                    list.add(user);
                }
            }

            return list;

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query!\n");
        }
    }

    private User findUser(Long from) {
        try {
            statement = connection.prepareStatement("SELECT first_name, last_name,email,password from users where uid = ?");
            statement.setLong(1, from);

            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                return null;
            }

            String firstName = resultSet.getString(1);
            String lastName = resultSet.getString(2);
            String email = resultSet.getString(3);
            String password = resultSet.getString(4);

            User user = new User(firstName, lastName, email, password);
            user.setId(from);
            return user;
        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query!\n");
        }
    }

    @Override
    public Page<Message> findAll(Pageable pageable) {
        Paginator<Message> paginator = new Paginator<>(pageable, this.findAll());
        return paginator.paginate();
    }
}
