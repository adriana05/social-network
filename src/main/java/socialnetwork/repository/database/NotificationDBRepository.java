package socialnetwork.repository.database;

import socialnetwork.domain.Notification;
import socialnetwork.repository.Repository;
import socialnetwork.repository.exceptions.RepositoryExceptions;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class NotificationDBRepository implements Repository<Long, Notification> {
    private final String url;
    private final String username;
    private final String password;
    private Connection connection;
    private PreparedStatement statement;

    public NotificationDBRepository(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;

        getConnection();

    }

    private void getConnection() {
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not connect to the DB");
        }
    }

    @Override
    public Notification findOne(Long aLong) {
        try {
            statement = connection.prepareStatement("SELECT text,is_opened, uid, eid, date_notification from notification where nid = ?");
            statement.setLong(1, aLong);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                return null;
            }

            String text = resultSet.getString(1);
            boolean isOpened = resultSet.getBoolean(2);
            Long uid = resultSet.getLong(3);
            Long eid = resultSet.getLong(4);
            LocalDateTime localDateTime = resultSet.getTimestamp(5).toLocalDateTime();

            Notification notification = new Notification(uid, text, isOpened, eid, localDateTime);
            notification.setId(aLong);

            return notification;

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query\n");
        }

    }

    @Override
    public Iterable<Notification> findAll() {
        List<Notification> notifications = new ArrayList<>();
        try {
            statement = connection.prepareStatement("SELECT * FROM notification");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long nid = resultSet.getLong(1);
                String text = resultSet.getString(2);
                boolean isOpened = resultSet.getBoolean(3);
                Long uid = resultSet.getLong(4);
                Long eid = resultSet.getLong(5);
                LocalDateTime localDateTime = resultSet.getTimestamp(6).toLocalDateTime();

                Notification notification = new Notification(uid, text, isOpened, eid, localDateTime);
                notification.setId(nid);

                notifications.add(notification);

            }

            return notifications;

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query\n");
        }
    }

    @Override
    public Notification save(Notification entity) {
        if (entity == null) {
            throw new RepositoryExceptions("Entity cannot be null");
        }

        try {
            statement = connection.prepareStatement("INSERT INTO notification(text,is_opened,uid,eid,date_notification) values(?,?,?,?,?)");
            statement.setString(1, entity.getText());
            statement.setBoolean(2, entity.isOpened());
            statement.setLong(3, entity.getIdUser());
            statement.setLong(4, entity.getIdEvent());
            statement.setTimestamp(5, Timestamp.valueOf(entity.getDate()));
            statement.executeUpdate();

            statement = connection.prepareStatement("SELECT MAX(nid) from notification");
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            entity.setId(resultSet.getLong(1));
            return entity;


        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query\n");
        }
    }

    @Override
    public Notification delete(Long aLong) {

        Notification notification = findOne(aLong);
        if (notification == null)
            throw new RepositoryExceptions("Entity not found!");

        try {
            statement = connection.prepareStatement("DELETE FROM notification where nid = ?");
            statement.setLong(1, aLong);
            statement.executeUpdate();

            return notification;

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query!");
        }
    }

    @Override
    public Notification update(Notification entity) {
        if (entity == null) {
            throw new RepositoryExceptions("Entity cannot be null!\n");
        }

        Notification notification = findOne(entity.getId());
        if (notification == null) {
            throw new RepositoryExceptions("Could not find the entity\n");
        }

        try {
            statement = connection.prepareStatement("UPDATE notification set is_opened = ?  where nid = ?");

            statement.setBoolean(1, true);
            statement.setLong(2, entity.getId());

            statement.executeUpdate();

            return entity;
        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query\n");
        }

    }
}
