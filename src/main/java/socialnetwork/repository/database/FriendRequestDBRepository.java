package socialnetwork.repository.database;

import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.Tuple;
import socialnetwork.repository.exceptions.RepositoryExceptions;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class FriendRequestDBRepository implements PagingRepository<Tuple<Long, Long>, FriendRequest> {
    private final String url;
    private final String username;
    private final String password;
    private Connection connection;
    private PreparedStatement statement;

    public FriendRequestDBRepository(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;

        getConnection();

    }

    private void getConnection() {
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not connect to the DB");
        }
    }


    @Override
    public FriendRequest findOne(Tuple<Long, Long> id) {

        if (id == null) {
            throw new RepositoryExceptions("Id cannot be null");
        }

        try {
            statement = connection.prepareStatement("SELECT id1, id2, status, date from friend_request where id1 = ? and id2 = ?");
            statement.setLong(1, id.getLeft());
            statement.setLong(2, id.getRight());

            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                return null;
            }

            Long id1 = resultSet.getLong(1);
            Long id2 = resultSet.getLong(2);
            String status = resultSet.getString(3);
            LocalDateTime dateTime = resultSet.getTimestamp(4).toLocalDateTime();

            return new FriendRequest(new Tuple<>(id1, id2), status, dateTime);

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query\n");
        }

    }

    @Override
    public Iterable<FriendRequest> findAll() {
        List<FriendRequest> list = new ArrayList<>();

        try {
            statement = connection.prepareStatement("SELECT * from friend_request");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long id1 = resultSet.getLong(1);
                Long id2 = resultSet.getLong(2);
                String status = resultSet.getString(3);
                LocalDateTime dateTime = resultSet.getTimestamp(4).toLocalDateTime();

                FriendRequest friendRequest = new FriendRequest(new Tuple<>(id1, id2), status, dateTime);
                list.add(friendRequest);
            }

            return list;

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query!\n");
        }
    }

    @Override
    public FriendRequest save(FriendRequest entity) {
        if (entity == null) {
            throw new RepositoryExceptions("Entity cannot be null!\n");
        }

        try {
            statement = connection.prepareStatement("INSERT INTO friend_request(id1,id2,status, date ) values(?,?,?,?)");
            statement.setLong(1, entity.getId().getLeft());
            statement.setLong(2, entity.getId().getRight());
            statement.setString(3, entity.getStatus());
            statement.setTimestamp(4, Timestamp.valueOf(LocalDateTime.now()));

            statement.executeUpdate();
            return entity;

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query!");
        }

    }

    @Override
    public FriendRequest delete(Tuple<Long, Long> id) {

        if (id == null) {
            throw new RepositoryExceptions("Id cannot be null\n");
        }

        FriendRequest friendRequest = findOne(id);
        if (friendRequest == null) {
            throw new RepositoryExceptions("Entity does not exist!\n");
        }

        if (!friendRequest.getStatus().equals("pending")) {
            throw new RepositoryExceptions("The request is no longer available!\n");
        }
        try {
            statement = connection.prepareStatement("Delete from friend_request where id1=? and id2=?");
            statement.setLong(1, friendRequest.getId().getLeft());
            statement.setLong(2, friendRequest.getId().getRight());

            statement.executeUpdate();

            return friendRequest;

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query!\n");
        }
    }

    @Override
    public FriendRequest update(FriendRequest entity) {

        if (entity == null) {
            throw new RepositoryExceptions("Entity cannot be null!\n");
        }

        FriendRequest friendRequest = findOne(entity.getId());
        if (friendRequest == null) {
            throw new RepositoryExceptions("Could not find the entity\n");
        }

        try {
            statement = connection.prepareStatement("UPDATE friend_request set status = ? , date = ?  where id1 =? and id2 = ?");
            statement.setString(1, entity.getStatus());
            statement.setTimestamp(2, Timestamp.valueOf(LocalDateTime.now()));
            statement.setLong(3, entity.getId().getLeft());
            statement.setLong(4, entity.getId().getRight());

            statement.executeUpdate();

            return entity;
        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query\n");
        }

    }


    @Override
    public Page<FriendRequest> findAll(Pageable pageable) {
        Paginator<FriendRequest> paginator = new Paginator<>(pageable, this.findAll());
        return paginator.paginate();
    }
}
