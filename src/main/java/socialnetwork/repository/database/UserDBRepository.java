package socialnetwork.repository.database;


import socialnetwork.domain.User;
import socialnetwork.repository.exceptions.RepositoryExceptions;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class UserDBRepository implements PagingRepository<Long, User> {
    private final String url;
    private final String username;
    private final String password;
    Connection connection;
    PreparedStatement statement;

    public UserDBRepository(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;

        getConnection();
    }

    private void getConnection() {
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not connect to the DB\n");
        }
    }

    @Override
    public User findOne(Long aLong) {

        try {
            statement = connection.prepareStatement("SELECT * from users where UID = ?");
            statement.setLong(1, aLong);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                return null;
            }

            Long id = resultSet.getLong("UID");
            String firstName = resultSet.getString("first_name");
            String secondName = resultSet.getString("last_name");
            String email = resultSet.getString("email");
            String password = resultSet.getString("password");

            User user = new User(firstName, secondName, email, password);
            user.setId(id);

            return user;

        } catch (SQLException e) {
            throw new RepositoryExceptions("Could not execute Query!\n");
        }

    }

    @Override
    public Iterable<User> findAll() {
        List<User> users = new ArrayList<>();
        try {
            statement = connection.prepareStatement("SELECT * from users ");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long id = resultSet.getLong("UID");
                String firstName = resultSet.getString("first_name");
                String lastName = resultSet.getString("last_name");
                String email = resultSet.getString("email");
                String password = resultSet.getString("password");

                User user = new User(firstName, lastName, email, password);
                user.setId(id);
                users.add(user);
            }
            return users;
        } catch (SQLException e) {
            throw new RepositoryExceptions("Could not execute Query!\n");
        }
    }

    @Override
    public User save(User entity) {
        final String firstName = entity.getFirstName();
        final String lastName = entity.getLastName();
        final Long id = entity.getId();

        try {
            statement = connection.prepareStatement("INSERT INTO users ( UID, first_name, last_name,email,password) values ( ?, ?, ?,?,?);");
            statement.setLong(1, id);
            statement.setString(2, firstName);
            statement.setString(3, lastName);
            statement.setString(4, entity.getEmail());
            statement.setString(5, entity.getPassword());
            statement.executeUpdate();

            return entity;

        } catch (SQLException e) {
            throw new RepositoryExceptions("This email is already being used!\n");
        }
    }

    @Override
    public User delete(Long aLong) {
        if (aLong == null)
            throw new RepositoryExceptions("Id cannot be null");

        User user = findOne(aLong);
        if (user == null) {
            throw new RepositoryExceptions("Entity not found!");
        }

        try {
            statement = connection.prepareStatement("Delete from users where UID = ?");
            statement.setLong(1, aLong);
            statement.executeUpdate();

            return user;

        } catch (SQLException e) {
            throw new RepositoryExceptions("Could not execute Query!\n");
        }
    }

    @Override
    public User update(User entity) {
        if (entity == null)
            throw new RepositoryExceptions("Entity cannot be null");

        User user = findOne(entity.getId());

        if (user == null)
            throw new RepositoryExceptions("Entity not found!");

        try {
            statement = connection.prepareStatement("UPDATE users set first_name = ?, set las_name = ? where UID = ?");
            statement.setString(1, entity.getFirstName());
            statement.setString(2, entity.getLastName());
            statement.setLong(3, entity.getId());

            statement.executeUpdate();
            return user;
        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute Query!\n");
        }
    }

    @Override
    public Page<User> findAll(Pageable pageable) {
        Paginator<User> paginator = new Paginator<>(pageable, this.findAll());
        return paginator.paginate();
    }
}
