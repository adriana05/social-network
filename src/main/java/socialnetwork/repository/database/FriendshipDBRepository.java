package socialnetwork.repository.database;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.repository.exceptions.RepositoryExceptions;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

public class FriendshipDBRepository implements PagingRepository<Tuple<Long, Long>, Friendship> {
    private final String url;
    private final String username;
    private final String password;
    private Connection connection;
    private PreparedStatement statement;

    public FriendshipDBRepository(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;

        getConnection();

    }

    private void getConnection() {
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not connect to the DB");
        }
    }

    @Override
    public Friendship findOne(Tuple<Long, Long> aLong) {

        try {
            statement = connection.prepareStatement("SELECT U1ID, U2ID , date from friendships where U1ID = ? and U2ID = ? or U1ID = ? and U2ID = ?");
            statement.setLong(1, aLong.getLeft());
            statement.setLong(2, aLong.getRight());
            statement.setLong(3, aLong.getRight());
            statement.setLong(4, aLong.getLeft());

            ResultSet resultSet = statement.executeQuery();


            if (!resultSet.next())
                return null;

            Long idLeft = resultSet.getLong("U1ID");
            Long idRight = resultSet.getLong("U2ID");
            LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();

            return new Friendship(new Tuple<>(idLeft, idRight), date);

        } catch (SQLException e) {
            throw new RepositoryExceptions("Could not execute Query");
        }


    }

    @Override
    public Iterable<Friendship> findAll() {
        Set<Friendship> friendships = new HashSet<>();
        try {
            statement = connection.prepareStatement("SELECT * from friendships ");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long idLeft = resultSet.getLong("U1ID");
                Long idRight = resultSet.getLong("U2ID");
                LocalDateTime date = resultSet.getTimestamp("date").toLocalDateTime();

                Friendship friendship = new Friendship(new Tuple<>(idLeft, idRight), date);

                friendships.add(friendship);
            }
            return friendships;
        } catch (SQLException e) {
            throw new RepositoryExceptions("Could not execute Query");
        }
    }


    @Override
    public Friendship save(Friendship entity) {
        if (entity == null)
            throw new RepositoryExceptions("Entity cannot be null");

        Friendship friendship = findOne(entity.getId());

        if (friendship != null) {
            throw new RepositoryExceptions("Entity already exist!");
        }

        final Long leftId = entity.getId().getLeft();
        final Long rightId = entity.getId().getRight();
        final LocalDateTime date = entity.getDate();
        try {
            statement = connection.prepareStatement("INSERT INTO friendships ( U1ID, U2ID, date) values ( " + leftId + "," + rightId + ",'" + date + "')");
            statement.executeUpdate();

            return entity;

        } catch (SQLException e) {
            throw new RepositoryExceptions("Could not execute Query");
        }

    }


    @Override
    public Friendship delete(Tuple<Long, Long> id) {
        if (id == null)
            throw new RepositoryExceptions("Id cannot be null");

        Friendship friendship = findOne(id);
        if (friendship == null) {
            throw new RepositoryExceptions("Entity not found!");
        }

        try {
            statement = connection.prepareStatement("Delete from friendships where U1ID = ? and U2ID = ?");
            statement.setLong(1, friendship.getId().getLeft());
            statement.setLong(2, friendship.getId().getRight());
            statement.executeUpdate();

            return friendship;

        } catch (SQLException e) {
            throw new RepositoryExceptions("Could not execute Query");
        }

    }

    @Override
    public Friendship update(Friendship entity) {
        if (entity == null)
            throw new RepositoryExceptions("Entity cannot be null");

        Friendship friendship = findOne(entity.getId());

        if (friendship == null)
            throw new RepositoryExceptions("Entity not found!");

        try {
            statement = connection.prepareStatement("UPDATE friendships set date =" + entity.getDate());

            statement.executeUpdate();
            return friendship;
        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not connect to DB");
        }

    }

    @Override
    public Page<Friendship> findAll(Pageable pageable) {
        Paginator<Friendship> paginator = new Paginator<>(pageable, this.findAll());
        return paginator.paginate();
    }
}
