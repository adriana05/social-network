package socialnetwork.repository.database;

import socialnetwork.domain.Participant;
import socialnetwork.domain.Tuple;
import socialnetwork.repository.Repository;
import socialnetwork.repository.exceptions.RepositoryExceptions;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ParticipantRepository implements Repository<Tuple<Long, Long>, Participant> {
    private final String url;
    private final String username;
    private final String password;
    private Connection connection;
    private PreparedStatement statement;

    public ParticipantRepository(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;

        getConnection();

    }

    private void getConnection() {
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not connect to the DB");
        }
    }

    @Override
    public Participant findOne(Tuple<Long, Long> id) {
        try {
            statement = connection.prepareStatement("SELECT notifications from participants where pid = ? and eid = ?");
            statement.setLong(1, id.getLeft());
            statement.setLong(2, id.getRight());

            ResultSet resultSet = statement.executeQuery();
            if (!resultSet.next())
                return null;

            Boolean notifications = resultSet.getBoolean(1);
            return new Participant(id.getLeft(), id.getRight(), notifications);

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query\n");
        }
    }

    @Override
    public Iterable<Participant> findAll() {
        List<Participant> participants = new ArrayList<>();
        try {
            statement = connection.prepareStatement("SELECT *  from participants");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long idUser = resultSet.getLong(1);
                Long idEvent = resultSet.getLong(2);
                Boolean notification = resultSet.getBoolean(3);

                participants.add(new Participant(idUser, idEvent, notification));

            }

            return participants;

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query\n");
        }
    }

    @Override
    public Participant save(Participant entity) {
        if (entity == null) {
            throw new RepositoryExceptions("Entity cannot be null");
        }

        try {
            statement = connection.prepareStatement("INSERT INTO participants(pid,eid,notifications) values(?,?,?)");
            statement.setLong(1, entity.getIdUser());
            statement.setLong(2, entity.getIdEvent());
            statement.setBoolean(3, entity.getNotification());

            statement.executeUpdate();
            return entity;

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query\n");
        }
    }

    @Override
    public Participant delete(Tuple<Long, Long> id) {
        Participant participant = findOne(id);
        if (participant == null)
            throw new RepositoryExceptions("Entity do not exist");

        try {
            statement = connection.prepareStatement("DELETE FROM participants where pid = ? and eid = ?");
            statement.setLong(1, id.getLeft());
            statement.setLong(2, id.getRight());
            statement.executeUpdate();

            return participant;
        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query\n");
        }
    }

    @Override
    public Participant update(Participant entity) {
        Participant participant = findOne(entity.getId());
        if (participant == null)
            throw new RepositoryExceptions("Entity not found");

        try {
            statement = connection.prepareStatement("UPDATE participants set notifications = ? where pid = ? and eid = ?");
            statement.setBoolean(1, entity.getNotification());
            statement.setLong(2, entity.getIdUser());
            statement.setLong(3, entity.getIdEvent());

            statement.executeUpdate();
            return participant;

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query\n");
        }
    }
}
