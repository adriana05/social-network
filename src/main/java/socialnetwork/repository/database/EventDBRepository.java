package socialnetwork.repository.database;

import socialnetwork.domain.IncomingEvent;
import socialnetwork.domain.User;
import socialnetwork.repository.exceptions.RepositoryExceptions;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.repository.paging.PagingRepository;

import java.sql.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class EventDBRepository implements PagingRepository<Long, IncomingEvent> {
    private final String url;
    private final String username;
    private final String password;
    private Connection connection;
    private PreparedStatement statement;

    public EventDBRepository(String url, String username, String password) {
        this.url = url;
        this.username = username;
        this.password = password;

        getConnection();

    }

    private void getConnection() {
        try {
            connection = DriverManager.getConnection(url, username, password);
        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not connect to the DB");
        }
    }

    private List<User> getEventParticipants(Long idEvent) {
        List<Long> idsParticipants = new ArrayList<>();

        try {
            statement = connection.prepareStatement("SELECT pid from participants where eid = ?");
            statement.setLong(1, idEvent);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long id = resultSet.getLong(1);
                idsParticipants.add(id);
            }

            return getUsersFromIds(idsParticipants);

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query\n");
        }
    }

    private List<User> getUsersFromIds(List<Long> idsParticipants) {
        List<User> users = new ArrayList<>();

        idsParticipants.forEach(x -> {
            try {
                statement = connection.prepareStatement("SELECT last_name,first_name,email,password from users where uid = ? ");
                statement.setLong(1, x);
                ResultSet resultSet = statement.executeQuery();

                resultSet.next();

                String lastName = resultSet.getString(1);
                String firstName = resultSet.getString(2);
                String email = resultSet.getString(3);
                String password = resultSet.getString(4);

                User user = new User(firstName, lastName, email, password);
                user.setId(x);

                users.add(user);

            } catch (SQLException sqlException) {
                throw new RepositoryExceptions("Could not execute the Query\n");
            }
        });
        return users;
    }

    @Override
    public IncomingEvent findOne(Long aLong) {
        try {
            statement = connection.prepareStatement("SELECT title,description,date_event,location from events where eid = ?");
            statement.setLong(1, aLong);
            ResultSet resultSet = statement.executeQuery();

            if (!resultSet.next()) {
                return null;
            }

            String title = resultSet.getString(1);
            String description = resultSet.getString(2);
            LocalDateTime date = resultSet.getTimestamp(3).toLocalDateTime();
            String location = resultSet.getString(4);

            List<User> going = getEventParticipants(aLong);

            IncomingEvent event = new IncomingEvent(title, description, going, date, location);
            event.setId(aLong);

            return event;

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query\n");
        }
    }

    @Override
    public Iterable<IncomingEvent> findAll() {
        List<IncomingEvent> events = new ArrayList<>();
        try {
            statement = connection.prepareStatement("SELECT * FROM events");
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                Long eid = resultSet.getLong(1);
                String title = resultSet.getString(2);
                String description = resultSet.getString(3);
                LocalDateTime date = resultSet.getTimestamp(4).toLocalDateTime();
                String location = resultSet.getString(5);
                List<User> going = getEventParticipants(eid);

                IncomingEvent incomingEvent = new IncomingEvent(title, description, going, date, location);
                incomingEvent.setId(eid);

                events.add(incomingEvent);

            }

            return events;

        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query\n");
        }
    }

    @Override
    public IncomingEvent save(IncomingEvent entity) {
        if (entity == null) {
            throw new RepositoryExceptions("Entity cannot be null");
        }

        try {
            statement = connection.prepareStatement("INSERT INTO events(title,description,date_event,location) values(?,?,?,?)");
            statement.setString(1, entity.getTitle());
            statement.setString(2, entity.getDescription());
            statement.setTimestamp(3, Timestamp.valueOf(entity.getDate()));
            statement.setString(4, entity.getLocation());
            statement.executeUpdate();

            statement = connection.prepareStatement("SELECT MAX(eid) from events");
            ResultSet resultSet = statement.executeQuery();
            resultSet.next();

            entity.setId(resultSet.getLong(1));
            return entity;


        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query\n");
        }
    }

    @Override
    public IncomingEvent delete(Long aLong) {

        IncomingEvent incomingEvent = findOne(aLong);
        if (incomingEvent == null) {
            throw new RepositoryExceptions("Entity not found");
        }

        try {
            statement = connection.prepareStatement("DELETE FROM events WHERE eid = ?");
            statement.setLong(1, aLong);

            statement.executeUpdate();
            return incomingEvent;


        } catch (SQLException sqlException) {
            throw new RepositoryExceptions("Could not execute the Query\n");
        }
    }

    @Override
    public IncomingEvent update(IncomingEvent entity) {
        return null;
    }

    @Override
    public Page<IncomingEvent> findAll(Pageable pageable) {
        Paginator<IncomingEvent> paginator = new Paginator<>(pageable, this.findAll());
        return paginator.paginate();
    }
}
