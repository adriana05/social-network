package socialnetwork.utils;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;
import socialnetwork.domain.User;
import socialnetwork.service.ServiceNotification;

public class NotifyTask {

    ServiceNotification serviceNotification;
    User user;
    Timeline timeline;

    public NotifyTask(ServiceNotification serviceNotification, User user) {
        this.serviceNotification = serviceNotification;
        this.user = user;

        timeline = new Timeline(new KeyFrame(Duration.minutes(0.5), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                serviceNotification.sendNotification(user);
            }
        }));
        timeline.setCycleCount(Timeline.INDEFINITE);
        timeline.play();
    }

}
