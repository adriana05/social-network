package socialnetwork.utils.observers;

import socialnetwork.utils.event.Event;

public interface Observer<E extends Event> {
    void update(E e);
}