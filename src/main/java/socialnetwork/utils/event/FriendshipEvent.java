package socialnetwork.utils.event;

public class FriendshipEvent implements Event {
    private EventFunctions type;

    public FriendshipEvent(EventFunctions type) {
        this.type = type;
    }

    public EventFunctions getType() {
        return type;
    }

    @Override
    public EventType getEvent() {
        return EventType.FRIENDSHIP_EVENT;
    }
}