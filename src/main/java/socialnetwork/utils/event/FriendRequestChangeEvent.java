package socialnetwork.utils.event;


public class FriendRequestChangeEvent implements Event {
    private EventFunctions type;

    public FriendRequestChangeEvent(EventFunctions type) {
        this.type = type;
    }

    public EventFunctions getType() {
        return type;
    }

    public EventType getEvent() {
        return EventType.FRIEND_REQUEST_EVENT;
    }
}

