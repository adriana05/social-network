package socialnetwork.utils.event;

public interface Event {
    EventType getEvent();
}
