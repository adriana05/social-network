package socialnetwork.utils.event;

public enum EventType {
    FRIEND_REQUEST_EVENT, MESSAGE_EVENT, FRIENDSHIP_EVENT, INCOMING_EVENT, NOTIFICATION;

}
