package socialnetwork.utils.event;

public class NotificationEvent implements Event {
    private EventFunctions type;

    public NotificationEvent(EventFunctions type) {
        this.type = type;
    }

    public EventFunctions getType() {
        return type;
    }

    public EventType getEvent() {
        return EventType.NOTIFICATION;
    }
}