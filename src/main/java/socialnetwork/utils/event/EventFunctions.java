package socialnetwork.utils.event;

public enum EventFunctions {
    ACCEPT, REJECT, DELETE, SEND
}
