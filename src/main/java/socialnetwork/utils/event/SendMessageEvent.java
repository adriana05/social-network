package socialnetwork.utils.event;

public class SendMessageEvent implements Event {
    private EventFunctions type;

    public SendMessageEvent(EventFunctions type) {
        this.type = type;
    }

    public EventFunctions getType() {
        return type;
    }

    public EventType getEvent() {
        return EventType.MESSAGE_EVENT;
    }
}
