package socialnetwork.utils.event;

public class CreateIncomingEvent implements Event {
    private EventFunctions type;

    public CreateIncomingEvent(EventFunctions type) {
        this.type = type;
    }

    public EventFunctions getType() {
        return type;
    }

    public EventType getEvent() {
        return EventType.INCOMING_EVENT;
    }
}