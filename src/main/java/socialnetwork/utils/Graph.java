package socialnetwork.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Graph {

    List<List<Integer>> adjListArray;
    int numberComponents;
    Map<Long, Integer> idFriends;
    List<List<Integer>> biggestComponent;

    public Graph(ArrayList<Long> listId, ArrayList<Long> listFriendships) {

        numberComponents = 0;
        int index = 0;
        idFriends = new HashMap<>();
        biggestComponent = new ArrayList<>();

        for (Long id : listId) {
            idFriends.put(id, index++);
        }

        adjListArray = new ArrayList<>();

        for (int i = 0; i < listId.size(); i++) {
            adjListArray.add(new ArrayList<>());
        }

        for (int i = 0; i < listFriendships.size() - 1; i += 2) {
            adjListArray.get(idFriends.get(listFriendships.get(i))).add(idFriends.get(listFriendships.get(i + 1)));
            adjListArray.get(idFriends.get(listFriendships.get(i + 1))).add(idFriends.get(listFriendships.get(i)));

        }

    }

    public List<Long> getBiggestComponent() {
        int index = 0;
        connectedComponents();
        int maxim = 0;

        for (int i = 0; i < biggestComponent.size(); i++) {
            if (biggestComponent.get(i).size() > maxim) {
                maxim = biggestComponent.get(i).size();
                index = i;
            }
        }

        List<Long> listIds = new ArrayList<>();

        for (int i = 0; i < biggestComponent.get(index).size(); i++) {
            final int value = biggestComponent.get(index).get(i);
            idFriends.forEach((key, value1) -> {
                if (value1 == value)
                    listIds.add(key);
            });
        }

        return listIds;
    }

    public int getNumberComponents() {
        return numberComponents;
    }

    public void DFS(int v, boolean[] visited, int numberComponents) {
        visited[v] = true;
        biggestComponent.get(numberComponents).add(v);
        for (int x : adjListArray.get(v)) {
            if (!visited[x]) DFS(x, visited, numberComponents);
        }

    }

    public void connectedComponents() {
        boolean[] visited = new boolean[idFriends.size()];
        for (int v = 0; v < idFriends.size(); ++v) {
            if (!visited[v]) {
                biggestComponent.add(new ArrayList<>());
                DFS(v, visited, numberComponents);
                numberComponents++;
            }
        }
    }

}