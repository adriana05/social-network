package socialnetwork;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import socialnetwork.config.ApplicationContext;
import socialnetwork.controller.ControllerAccount;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.*;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.service.*;

import java.io.IOException;

public class MainFX extends Application {

    String url;
    String username;
    String password;

    Validator<User> userValidator;
    Validator<FriendRequest> friendRequestValidator;
    Validator<Friendship> friendshipValidator;
    Validator<Message> messageValidator;

    PagingRepository<Long, User> userDBRepository;
    PagingRepository<Tuple<Long, Long>, Friendship> friendshipDBRepository;
    PagingRepository<Tuple<Long, Long>, FriendRequest> friendRequestDBRepository;
    PagingRepository<Long, Message> messageDBRepository;
    PagingRepository<Long, IncomingEvent> incomingEventRepository;
    Repository<Tuple<Long, Long>, Participant> participantRepository;
    Repository<Long, Notification> notificationRepository;

    ServiceFriendship serviceFriendship;
    ServiceUser serviceUser;
    ServiceFriendRequest serviceFriendRequest;
    ServiceMessage serviceMessage;
    ServiceEvent serviceEvent;
    ServiceNotification serviceNotification;

    public static void main(String[] args) {
        launch(args);
    }

    private void initialize() {
        url = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url");
        username = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username");
        password = ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password");

        userValidator = new ValidatorUser();
        friendRequestValidator = new ValidatorFriendRequest();
        friendshipValidator = new ValidatorFriendship();
        messageValidator = new ValidatorMessage();

        userDBRepository = new UserDBRepository(url, username, password);
        friendshipDBRepository = new FriendshipDBRepository(url, username, password);
        friendRequestDBRepository = new FriendRequestDBRepository(url, username, password);
        messageDBRepository = new MessageRepository(url, username, password);
        incomingEventRepository = new EventDBRepository(url, username, password);
        participantRepository = new ParticipantRepository(url, username, password);
        notificationRepository = new NotificationDBRepository(url, username, password);

        serviceFriendship = new ServiceFriendship(friendshipDBRepository, friendshipValidator);
        serviceUser = new ServiceUser(userDBRepository, friendshipDBRepository, userValidator, friendshipValidator);
        serviceFriendRequest = new ServiceFriendRequest(friendRequestDBRepository, friendshipDBRepository, userDBRepository, friendRequestValidator, friendshipValidator);
        serviceMessage = new ServiceMessage(messageDBRepository, userDBRepository, friendshipDBRepository, messageValidator, userValidator);
        serviceEvent = new ServiceEvent(incomingEventRepository, participantRepository, notificationRepository);
        serviceNotification = new ServiceNotification(notificationRepository, incomingEventRepository, participantRepository);
    }

    @Override
    public void start(Stage primaryStage) throws IOException {
        initialize();

        initView(primaryStage);
//        Stage secondStage = new Stage();
//        initView(secondStage);
        primaryStage.show();


    }

    private void initView(Stage primaryStage) throws IOException {

        FXMLLoader applicationSignIn = new FXMLLoader();
        applicationSignIn.setLocation(getClass().getResource("/views/start.fxml"));
        AnchorPane applicationLayout = applicationSignIn.load();

        Scene scene = new Scene(applicationLayout);
        primaryStage.setScene(scene);
        scene.setFill(Color.TRANSPARENT);
        primaryStage.initStyle(StageStyle.TRANSPARENT);

        ControllerAccount singInController = applicationSignIn.getController();
        singInController.setUp(serviceUser, serviceFriendRequest, serviceMessage, serviceFriendship, serviceEvent, serviceNotification, primaryStage);

        primaryStage.show();

    }
}
