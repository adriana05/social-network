package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.service.exceptions.ServiceExceptions;
import socialnetwork.utils.event.Event;
import socialnetwork.utils.event.EventFunctions;
import socialnetwork.utils.event.FriendRequestChangeEvent;
import socialnetwork.utils.observers.Observable;
import socialnetwork.utils.observers.Observer;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ServiceFriendRequest implements Observable<Event> {
    private final Validator<FriendRequest> friendRequestValidator;
    private final Validator<Friendship> friendshipValidator;
    Repository<Tuple<Long, Long>, FriendRequest> friendRequestRepository;
    Repository<Tuple<Long, Long>, Friendship> friendshipRepository;
    Repository<Long, User> userRepository;
    int pageSentFriendRequests = 0;
    int pageFriendRequests = 0;
    int sizeSentFriendRequests = 17;
    int sizeFriendRequestsPage = 14;
    private List<Observer<Event>> observers = new ArrayList<>();

    public ServiceFriendRequest(Repository<Tuple<Long, Long>, FriendRequest> friendRequestRepository, Repository<Tuple<Long, Long>, Friendship> friendshipRepository, Repository<Long, User> userRepository, Validator<FriendRequest> friendRequestValidator, Validator<Friendship> friendshipValidator) {
        this.friendRequestRepository = friendRequestRepository;
        this.friendshipRepository = friendshipRepository;
        this.friendRequestValidator = friendRequestValidator;
        this.friendshipValidator = friendshipValidator;
        this.userRepository = userRepository;

    }

    public FriendRequest add(User sentFriendRequest, User receivedFriendRequest) {
        FriendRequest entity = new FriendRequest(new Tuple<>(sentFriendRequest.getId(), receivedFriendRequest.getId()), "pending");
        friendRequestValidator.validate(entity);

        User user1 = userRepository.findOne(entity.getId().getLeft());
        User user2 = userRepository.findOne(entity.getId().getRight());

        if (user1 == null) {
            throw new ServiceExceptions("First user who tries to send the friend request not found");
        }

        if (user2 == null) {
            throw new ServiceExceptions("User not found");
        }

        Friendship friendship = friendshipRepository.findOne(new Tuple<>(entity.getId().getLeft(), entity.getId().getRight()));
        FriendRequest friendRequest = friendRequestRepository.findOne(entity.getId());
        FriendRequest friendRequestReverse = friendRequestRepository.findOne(new Tuple<>(entity.getId().getRight(), entity.getId().getLeft()));
        if (friendship == null) {
            if (friendRequest != null) {
                if (friendRequest.getStatus().equals("pending"))
                    throw new ServiceExceptions("You already sent a friend request to this person\n");

                FriendRequest fr = friendRequestRepository.update(entity);
                if (fr != null) {
                    notifyObservers(new FriendRequestChangeEvent(EventFunctions.SEND));
                }
            } else {
                if (friendRequestReverse != null) {
                    if (friendRequestReverse.getStatus().equals("pending"))
                        throw new ServiceExceptions("This person has already sent you a friend request\n");
                }
                FriendRequest fr = friendRequestRepository.save(entity);
                if (fr != null) {
                    notifyObservers(new FriendRequestChangeEvent(EventFunctions.SEND));
                }
            }
        }
        return entity;
    }

    public FriendRequest remove(Tuple<Long, Long> id) {
        return friendRequestRepository.delete(id);
    }

    public Iterable<FriendRequest> getAll() {
        return friendRequestRepository.findAll();
    }

    public FriendRequest findOne(Tuple<Long, Long> id) {
        return friendRequestRepository.findOne(id);
    }

    public FriendRequest acceptFriendRequest(FriendRequest accepted) {
        accepted.setStatus("accepted");
        FriendRequest friendRequest = friendRequestRepository.update(accepted);
        friendshipRepository.save(new Friendship(accepted.getId()));
        if (friendRequest != null) {
            notifyObservers(new FriendRequestChangeEvent(EventFunctions.ACCEPT));
        }

        return friendRequest;
    }

    public FriendRequest rejectFriendRequest(FriendRequest rejected) {
        rejected.setStatus("rejected");
        FriendRequest friendRequest = friendRequestRepository.update(rejected);
        if (friendRequest != null) {
            notifyObservers(new FriendRequestChangeEvent(EventFunctions.REJECT));
        }
        return friendRequest;
    }

    public FriendRequest cancelFriendRequest(User sentFriendRequest, User receivedFriendRequest) {

        FriendRequest friendRequest = friendRequestRepository.delete(new Tuple<>(sentFriendRequest.getId(), receivedFriendRequest.getId()));
        if (friendRequest != null) {
            notifyObservers(new FriendRequestChangeEvent(EventFunctions.DELETE));
        }
        return friendRequest;
    }

    public Iterable<UserFriendRequestDto> getUserFriendsRequest(Long id) {
        Iterable<FriendRequest> getAll = friendRequestRepository.findAll();
        Predicate<FriendRequest> filterByUser = x -> x.getId().getRight().equals(id);

        return StreamSupport.stream(getAll.spliterator(), false)
                .filter(filterByUser)
                .map(x -> new UserFriendRequestDto(x.getId(), userRepository.findOne(x.getId().getLeft()).getFirstName(), userRepository.findOne(x.getId().getLeft()).getLastName(), x.getStatus(), x.getDate()))
                .collect(Collectors.toList());

    }

    public Iterable<User> getSentFriendRequests(Long id) {
        Iterable<FriendRequest> friendRequests = friendRequestRepository.findAll();
        Predicate<FriendRequest> byUser = x -> x.getId().getLeft().equals(id) && x.getStatus().equals("pending");

        return StreamSupport.stream(friendRequests.spliterator(), false)
                .filter(byUser)
                .map(x -> userRepository.findOne(x.getId().getRight()))
                .collect(Collectors.toList());

    }

    public List<User> getNextSentFriendRequests(Long id) {
        this.pageSentFriendRequests++;
        List<User> friendRequests = StreamSupport.stream(getSentFriendRequests(id).spliterator(), false).collect(Collectors.toList());
        if (this.sizeSentFriendRequests >= friendRequests.size()) {
            this.pageSentFriendRequests = 0;
            return getSentFriendRequestsOnPage(0, id);
        }
        if (this.pageSentFriendRequests + this.sizeSentFriendRequests > friendRequests.size()) {
            this.pageSentFriendRequests--;
        }
        return getSentFriendRequestsOnPage(pageSentFriendRequests, id);
    }

    public List<User> getPreviousSentFriendRequests(Long id) {
        this.pageSentFriendRequests--;
        if (this.pageSentFriendRequests <= 0) {
            this.pageSentFriendRequests = 0;
        }
        return getSentFriendRequestsOnPage(pageSentFriendRequests, id);
    }

    public List<User> getSentFriendRequestsOnPage(int page, Long id) {
        this.pageSentFriendRequests = page;
        Pageable pageable = new PageableImplementation(page, this.sizeSentFriendRequests);
        Paginator<User> paginator = new Paginator<>(pageable, getSentFriendRequests(id));
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    @Override
    public void addObserver(Observer<Event> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<Event> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(Event t) {
        observers.stream().forEach(x -> x.update(t));
    }


    public boolean verifySentFriendRequest(User user1, User user2) {
        FriendRequest friendRequest = friendRequestRepository.findOne(new Tuple<>(user1.getId(), user2.getId()));
        return friendRequest != null && friendRequest.getStatus().equals("pending");
    }


    public List<UserFriendRequestDto> getNextFriendRequests(Long id) {
        this.pageFriendRequests++;
        List<UserFriendRequestDto> friendRequests = StreamSupport.stream(getUserFriendsRequest(id).spliterator(), false).collect(Collectors.toList());
        if (this.sizeFriendRequestsPage >= friendRequests.size()) {
            this.pageFriendRequests = 0;
            return getFriendRequestsOnPage(0, id);
        }
        if (this.pageFriendRequests + this.sizeFriendRequestsPage > friendRequests.size()) {
            this.pageFriendRequests--;
        }
        return getFriendRequestsOnPage(pageFriendRequests, id);
    }

    public List<UserFriendRequestDto> getPreviousFriendRequests(Long id) {
        this.pageFriendRequests--;
        if (this.pageFriendRequests <= 0) {
            this.pageFriendRequests = 0;
        }
        return getFriendRequestsOnPage(pageFriendRequests, id);
    }

    public List<UserFriendRequestDto> getFriendRequestsOnPage(int page, Long id) {
        this.pageFriendRequests = page;
        Pageable pageable = new PageableImplementation(page, this.sizeFriendRequestsPage);
        Paginator<UserFriendRequestDto> paginator = new Paginator<>(pageable, getUserFriendsRequest(id));
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public boolean verifyReceivedFriendRequest(User userLoggedIn, User selected) {
        FriendRequest friendRequest = friendRequestRepository.findOne(new Tuple<>(selected.getId(), userLoggedIn.getId()));
        return friendRequest != null && friendRequest.getStatus().equals("pending");
    }
}
