package socialnetwork.service;

import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.utils.Graph;
import socialnetwork.utils.event.Event;
import socialnetwork.utils.event.EventFunctions;
import socialnetwork.utils.event.FriendshipEvent;
import socialnetwork.utils.observers.Observable;
import socialnetwork.utils.observers.Observer;

import java.util.ArrayList;
import java.util.List;

public class ServiceFriendship implements Observable<Event> {

    private final Repository<Tuple<Long, Long>, Friendship> friendshipRepository;
    private final Validator<Friendship> friendshipValidator;
    private List<Observer<Event>> observers = new ArrayList<>();

    public ServiceFriendship(Repository<Tuple<Long, Long>, Friendship> friendshipRepositoryParam, Validator<Friendship> friendshipValidator) {

        this.friendshipRepository = friendshipRepositoryParam;
        this.friendshipValidator = friendshipValidator;

    }

    public Iterable<Friendship> getAll() {

        return friendshipRepository.findAll();
    }

    public Friendship remove(Tuple<Long, Long> id) {

        Friendship friendship = friendshipRepository.delete(id);
        notifyObservers(new FriendshipEvent(EventFunctions.DELETE));
        return friendship;
    }

    public Friendship modify(Friendship entity) {
        friendshipValidator.validate(entity);
        return friendshipRepository.update(entity);

    }

    Graph createGraph(ArrayList<Long> listId) {
        Iterable<Friendship> allFriendships = friendshipRepository.findAll();
        ArrayList<Long> listFriendships = new ArrayList<>();

        allFriendships.forEach(x -> {
            listFriendships.add(x.getId().getLeft());
            listFriendships.add(x.getId().getRight());
        });

        return new Graph(listId, listFriendships);
    }

    public int getNumberCommunities(ArrayList<Long> listId) {

        Graph graph = createGraph(listId);

        graph.connectedComponents();

        return graph.getNumberComponents();
    }

    public List<Long> mostSociableCommunity(ArrayList<Long> listId) {

        Graph graph = createGraph(listId);

        return graph.getBiggestComponent();
    }

    @Override
    public void addObserver(Observer<Event> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<Event> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(Event t) {
        observers.stream().forEach(x -> x.update(t));
    }
}


