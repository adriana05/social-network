package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.utils.event.CreateIncomingEvent;
import socialnetwork.utils.event.Event;
import socialnetwork.utils.event.EventFunctions;
import socialnetwork.utils.observers.Observable;
import socialnetwork.utils.observers.Observer;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ServiceEvent implements Observable<Event> {
    private final PagingRepository<Long, IncomingEvent> eventRepository;
    private final Repository<Tuple<Long, Long>, Participant> participantRepository;
    private final Repository<Long, Notification> notificationRepository;
    int page = 0;
    int size = 6;
    private List<Observer<Event>> observers = new ArrayList<>();

    public ServiceEvent(PagingRepository<Long, IncomingEvent> eventRepository, Repository<Tuple<Long, Long>, Participant> participantRepository, Repository<Long, Notification> notificationRepository) {
        this.eventRepository = eventRepository;
        this.participantRepository = participantRepository;
        this.notificationRepository = notificationRepository;
    }

    public IncomingEvent findOne(Long idEvent) {
        return eventRepository.findOne(idEvent);
    }

    public Participant subscribe(IncomingEvent incomingEvent, User user) {
        Participant participant = new Participant(user.getId(), incomingEvent.getId(), true);
        participant = participantRepository.save(participant);

        notifyObservers(new CreateIncomingEvent(EventFunctions.ACCEPT));
        return participant;
    }


    public Participant unsubscribe(IncomingEvent incomingEvent, User user) {
        Participant participant = participantRepository.delete(new Tuple<>(user.getId(), incomingEvent.getId()));
        notifyObservers(new CreateIncomingEvent(EventFunctions.DELETE));
        return participant;
    }

    public Participant getNotifications(IncomingEvent incomingEvent, User user) {
        Participant participant = participantRepository.update(new Participant(user.getId(), incomingEvent.getId(), true));

        return participant;
    }

    public Participant stopGettingNotifications(IncomingEvent incomingEvent, User user) {
        Participant participant = participantRepository.update(new Participant(user.getId(), incomingEvent.getId(), false));

        return participant;
    }

    public IncomingEvent addEvent(String title, String description, LocalDateTime date, String location, User user) {
        List<User> going = new ArrayList<>();
        going.add(user);
        IncomingEvent incomingEvent = new IncomingEvent(title, description, going, date, location);
        incomingEvent = eventRepository.save(incomingEvent);
        participantRepository.save(new Participant(user.getId(), incomingEvent.getId(), true));

        notifyObservers(new CreateIncomingEvent(EventFunctions.SEND));
        return incomingEvent;
    }

    public Iterable<IncomingEvent> findAllEvents() {
        return eventRepository.findAll();
    }

    public List<User> getAllAttendantsForAnEvent(IncomingEvent event) {
        IncomingEvent incomingEvent = eventRepository.findOne(event.getId());
        return incomingEvent.getGoing();
    }


    public List<IncomingEvent> getPreviousEvents() {
        this.page--;
        if (this.page <= 0) {
            this.page = 0;
        }
        return getEventsOnPage(page);
    }

    public List<IncomingEvent> getNextEvents() {
        this.page++;
        List<IncomingEvent> events = StreamSupport.stream(eventRepository.findAll().spliterator(), false).collect(Collectors.toList());
        if (this.size >= events.size()) {
            this.page = 0;
            return getEventsOnPage(0);
        }
        if (this.page + this.size > events.size()) {
            this.page--;
        }
        return getEventsOnPage(page);
    }

    public List<IncomingEvent> getEventsOnPage(int page) {
        this.page = page;
        Pageable pageable = new PageableImplementation(page, this.size);
        Page<IncomingEvent> eventPage = eventRepository.findAll(pageable);
        return eventPage.getContent().collect(Collectors.toList());
    }

    @Override
    public void addObserver(Observer<Event> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<Event> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(Event t) {
        observers.stream().forEach(x -> {
            x.update(t);
        });
    }

    public boolean checkSubscription(IncomingEvent event, User userLoggedIn) {
        return participantRepository.findOne(new Tuple<>(userLoggedIn.getId(), event.getId())) != null;
    }

    public boolean checkGettingNotification(IncomingEvent event, User userLoggedIn) {
        Participant participant = participantRepository.findOne(new Tuple<>(userLoggedIn.getId(), event.getId()));
        return participant != null && participant.getNotification();
    }


}
