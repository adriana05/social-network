package socialnetwork.service.exceptions;

public class ServiceExceptions extends RuntimeException {
    public ServiceExceptions() {
    }

    public ServiceExceptions(String message) {
        super(message);
    }


}
