package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.service.exceptions.ServiceExceptions;
import socialnetwork.utils.event.Event;
import socialnetwork.utils.event.EventFunctions;
import socialnetwork.utils.event.SendMessageEvent;
import socialnetwork.utils.observers.Observable;
import socialnetwork.utils.observers.Observer;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ServiceMessage implements Observable<Event> {
    private final Validator<Message> messageValidator;
    private final Validator<User> userValidator;
    Repository<Long, Message> messageRepository;
    Repository<Long, User> userRepository;
    Repository<Tuple<Long, Long>, Friendship> friendshipRepository;
    int pageMessage = 0;
    int sizePageMessage = 6;
    private List<Observer<Event>> observers = new ArrayList<>();
    private long id = 0;

    public ServiceMessage(Repository<Long, Message> messageRepository, Repository<Long, User> userRepository, Repository<Tuple<Long, Long>, Friendship> friendshipRepository, Validator<Message> messageValidator, Validator<User> userValidator) {
        this.messageRepository = messageRepository;
        this.userRepository = userRepository;
        this.friendshipRepository = friendshipRepository;
        this.messageValidator = messageValidator;
        this.userValidator = userValidator;
    }


    void generateFirstId() {
        Iterable<Message> messages = messageRepository.findAll();

        messages.forEach(x -> {
            if (x.getId() > id) {
                id = x.getId();
            }
        });
        id++;

    }

    void generateNextId() {
        if (id == 0) generateFirstId();
        else {
            id++;
        }
    }

    public Message add(Long from, List<Long> idsTO, String message) {
        User user = userRepository.findOne(from);
        if (user == null) {
            throw new ServiceExceptions("User not found!\n");
        }
        List<User> list = new ArrayList<>();

        idsTO.forEach(x -> {
            User userTo = userRepository.findOne(x);
            if (userTo == null) {
                throw new ServiceExceptions("User not found!\n");
            }
            list.add(userTo);
        });

        Message entity = new Message(user, list, message);
        messageValidator.validate(entity);
        generateNextId();
        entity.setId(id);

        Message m = messageRepository.save(entity);
        notifyObservers(new SendMessageEvent(EventFunctions.SEND));
        return m;
    }

    public Iterable<Message> getAll() {
        return messageRepository.findAll();
    }

    public Message findOne(Long id) {
        return messageRepository.findOne(id);
    }

    public Message remove(Long id) {
        return messageRepository.delete(id);
    }

    public Message update(Message entity) {

        messageValidator.validate(entity);
        return messageRepository.update(entity);
    }

    public void replyAll(Long idReply, Long idMessage, String message) {
        User replyUser = userRepository.findOne(idReply);
        Message messageOriginal = messageRepository.findOne(idMessage);

        if (replyUser == null) {
            throw new ServiceExceptions("User not found!\n");
        }
        if (messageOriginal == null) {
            throw new ServiceExceptions("Message not found!\n");
        }
        if (!messageOriginal.getTo().contains(replyUser)) {
            throw new ServiceExceptions("You cannot reply to a message that was not sent to you!\n");
        }

        List<User> to = messageOriginal.getTo();

        User user = messageOriginal.getFrom();

        to.add(user);
        to.remove(replyUser);

        ReplyMessage replyMessage = new ReplyMessage(replyUser, to, message, messageOriginal);
        messageValidator.validate(replyMessage);

        generateNextId();
        replyMessage.setId(id);

        messageRepository.save(replyMessage);
    }

    public void replyUser(Long idReply, Long idMessage, String message, Long idUser) {
        User replyUser = userRepository.findOne(idReply);
        Message messageOriginal = messageRepository.findOne(idMessage);

        if (replyUser == null) {
            throw new ServiceExceptions("User not found!\n");
        }
        if (messageOriginal == null) {
            throw new ServiceExceptions("Message not found!\n");
        }
        if (!messageOriginal.getTo().contains(replyUser)) {
            throw new ServiceExceptions("You cannot reply to a meesage that was not sent to you!\n");
        }

        List<User> to = new ArrayList<>();

        User user = userRepository.findOne(idUser);
        if (user == null) {
            throw new ServiceExceptions("User not found!\n");
        }
        to.add(user);

        ReplyMessage replyMessage = new ReplyMessage(replyUser, to, message, messageOriginal);
        messageValidator.validate(replyMessage);

        generateNextId();
        replyMessage.setId(id);

        messageRepository.save(replyMessage);
        notifyObservers(new SendMessageEvent(EventFunctions.SEND));

    }

    public List<Message> conversation(Long firstID, Long secondID) {
        List<Message> conversation = new ArrayList<>();
        User firstUser = userRepository.findOne(firstID);
        User secondUser = userRepository.findOne(secondID);

        if (firstUser == null || secondUser == null) {
            throw new ServiceExceptions("Users not found!\n");
        }

        Iterable<Message> list = messageRepository.findAll();

        List<Message> listMessages = new ArrayList<>();
        list.forEach(listMessages::add);


        Predicate<Message> fromFirstUser = x -> x.getFrom().getId().equals(firstID) && x.getTo().contains(secondUser);
        Predicate<Message> fromSecondUser = x -> x.getFrom().getId().equals(secondID) && x.getTo().contains(firstUser);

        conversation = listMessages.stream()
                .filter(x -> x.getTo().size() == 1)
                .filter(fromFirstUser.or(fromSecondUser))
                .collect(Collectors.toList());

        return conversation;
    }

    public List<Message> getReceivedUsersMessages(User user) {
        Iterable<Message> messages = messageRepository.findAll();

        Predicate<Message> byToList = x -> (x.getTo().contains(user) || x.getFrom().equals(user)) && x.getTo().size() > 1;

        List<Message> allMessages = StreamSupport.stream(messages.spliterator(), false)
                .filter(byToList)
                .collect(Collectors.toList());

        List<Message> lastMessagesReceivedFromGroupChat = allMessages.stream()
                .map(x -> getGroupConversation(x.getTo(), x.getFrom()))
                .map(x -> x.get(x.size() - 1))
                .collect(Collectors.toList());

        return lastMessagesReceivedFromGroupChat.stream().distinct().collect(Collectors.toList());
    }

    public List<Message> getGroupConversation(List<User> listRecipients, User user) {
        Iterable<Message> allMessages = messageRepository.findAll();
        List<Message> groupChat = new ArrayList<>();
        boolean found;
        for (Message message : allMessages) {
            found = true;
            if (listRecipients.size() != message.getTo().size())
                found = false;
            else if (!message.getTo().contains(user) && !message.getFrom().equals(user))
                found = false;
            else {
                for (User user1 : listRecipients) {
                    if (!message.getTo().contains(user1) && !message.getFrom().equals(user1)) {
                        found = false;
                        break;
                    }
                }
            }
            if (found)
                groupChat.add(message);
        }
        return groupChat;
    }

    public List<Message> reportAllMessages(LocalDate from, LocalDate to, User userLoggedIn) {

        Iterable<Message> allMessages = messageRepository.findAll();
        Predicate<Message> messageByTime = x -> x.getDate().isAfter(from.atStartOfDay()) && x.getDate().isBefore(to.atStartOfDay()) && x.getTo().contains(userLoggedIn);


        return StreamSupport.stream(allMessages.spliterator(), false)
                .filter(messageByTime)
                .sorted(Comparator.comparing(Message::getDate))
                .collect(Collectors.toList());


    }


    public List<Message> reportAllMessagesFromSpecificFriend(User user, User userLoggedIn, LocalDate from, LocalDate to) {

        Predicate<Message> messageByUser = x -> x.getTo().size() == 1 && x.getFrom().equals(user);

        return reportAllMessages(from, to, userLoggedIn).stream()
                .filter(messageByUser)
                .sorted(Comparator.comparing(Message::getDate))
                .collect(Collectors.toList());

    }

    @Override
    public void addObserver(Observer<Event> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<Event> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(Event t) {
        observers.stream().forEach(x -> x.update(t));
    }


    public List<Message> getNextMessages(User user) {
        this.pageMessage++;
        List<Message> messages = getReceivedUsersMessages(user);
        if (this.sizePageMessage >= messages.size()) {
            this.pageMessage = 0;
            return getMessagesOnPage(0, user);
        }
        if (this.pageMessage + this.sizePageMessage > messages.size()) {
            this.pageMessage--;
        }
        return getMessagesOnPage(pageMessage, user);
    }

    public List<Message> getPreviousMessages(User user) {
        this.pageMessage--;
        if (this.pageMessage <= 0) {
            this.pageMessage = 0;
        }
        return getMessagesOnPage(pageMessage, user);
    }

    public List<Message> getMessagesOnPage(int page, User user) {
        this.pageMessage = page;
        Pageable pageable = new PageableImplementation(page, this.sizePageMessage);
        Paginator<Message> paginator = new Paginator<>(pageable, getReceivedUsersMessages(user));
        return paginator.paginate().getContent().collect(Collectors.toList());
    }
}
