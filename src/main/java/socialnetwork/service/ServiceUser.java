package socialnetwork.service;

import socialnetwork.domain.FriendDTO;
import socialnetwork.domain.Friendship;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PageableImplementation;
import socialnetwork.repository.paging.Paginator;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.service.exceptions.ServiceExceptions;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class ServiceUser {
    private static long id = -1;
    private final PagingRepository<Long, User> userRepository;
    private final PagingRepository<Tuple<Long, Long>, Friendship> friendshipRepository;
    private final Validator<User> userValidator;
    private final Validator<Friendship> friendshipValidator;
    private final int sizeFriendsPage = 16;
    private final int sizeFindFriendsPage = 17;
    private int pageFriends = 0;
    private int pageFindFriends = 0;

    public ServiceUser(PagingRepository<Long, User> repositoryUserParam, PagingRepository<Tuple<Long, Long>, Friendship> repositoryFriendshipParam, Validator<User> userValidator, Validator<Friendship> friendshipValidator) {
        this.userRepository = repositoryUserParam;
        this.friendshipRepository = repositoryFriendshipParam;
        this.userValidator = userValidator;
        this.friendshipValidator = friendshipValidator;
    }

    void generateFirstId() {
        Iterable<User> users = userRepository.findAll();

        users.forEach(x -> {
            if (x.getId() > id) {
                id = x.getId();
            }
        });
        id++;

    }

    void generateNextId() {
        if (id == -1) generateFirstId();
        else {
            id++;
        }
    }

    public String hashPassword(String password) {

        String hashedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());

            byte[] bytes = md.digest();

            StringBuilder sb = new StringBuilder();
            for (byte aByte : bytes) {
                sb.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
            }

            hashedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return hashedPassword;

    }

    public User add(User user) {

        userValidator.validate(user);

        generateNextId();

        user.setId(id);
        user.setPassword(hashPassword(user.getPassword()));
        return userRepository.save(user);
    }

    public Iterable<User> getAll() {

        Iterable<User> list = userRepository.findAll();
        Iterable<Friendship> listFriends = friendshipRepository.findAll();

        list.forEach(x -> listFriends.forEach(y -> {
            if (y.getId().getLeft().equals(x.getId())) {
                x.addFriend(userRepository.findOne(y.getId().getRight()));
            } else if (y.getId().getRight().equals(x.getId())) {
                x.addFriend(userRepository.findOne(y.getId().getLeft()));
            }
        }));

        return list;
    }

    public User remove(Long id) {

        return userRepository.delete(id);
    }

    public User modify(User entity) {
        userValidator.validate(entity);
        return userRepository.update(entity);
    }

    public User findOneByEmail(String email, String password) {
        Iterable<User> allUsers = userRepository.findAll();
        for (User user : allUsers) {
            if (user.getEmail().equals(email) && user.getPassword().equals(hashPassword(password)))
                return user;
        }
        return null;
    }

    public User findOne(Long id) {
        User user = userRepository.findOne(id);

        if (user == null)
            throw new ValidationException("ID(s) not found");
        return user;
    }

    public List<FriendDTO> reportAllFriendships(LocalDate from, LocalDate to, User userLoggedIn) {

        List<FriendDTO> allFriends = user_friendsList(userLoggedIn.getId());
        Predicate<FriendDTO> friendshipByTime = x -> x.getDate().isAfter(from.atStartOfDay()) && x.getDate().isBefore(to.atStartOfDay());

        return allFriends.stream()
                .filter(friendshipByTime)
                .sorted(Comparator.comparing(FriendDTO::getDate))
                .collect(Collectors.toList());

    }

    public List<FriendDTO> user_friendsList(Long id) {
        List<FriendDTO> list = new ArrayList<>();

        User user = userRepository.findOne(id);

        if (user == null)
            throw new ServiceExceptions("Id not found");

        Stream<User> allUsers = StreamSupport.stream(getAll().spliterator(), false);

        Predicate<User> foundUser = x -> x.getId().equals(user.getId());

        allUsers.filter(foundUser)
                .collect(Collectors.toList())
                .get(0).getFriends()
                .stream()
                .map(x -> new FriendDTO(x.getId(), x.getFirstName(), x.getLastName(), friendshipRepository.findOne(new Tuple<>(id, x.getId())).getDate()))
                .forEach(list::add);

        return list;
    }

    public List<FriendDTO> user_friendsList_atSpecificMonth(Long id, Integer month) {

        List<FriendDTO> list = user_friendsList(id);
        List<FriendDTO> list_specificMonth;

        list_specificMonth = list.stream()
                .filter(x -> x.getDate().getMonthValue() == month)
                .collect(Collectors.toList());

        return list_specificMonth;
    }

    public Iterable<User> findFriends(Long id) {
        User user = userRepository.findOne(id);
        Iterable<User> allUsers = getAll();
        Predicate<User> byUnknownFriend = x -> !x.getId().equals(id) && !x.getFriends().contains(user);

        return StreamSupport.stream(allUsers.spliterator(), false)
                .filter(byUnknownFriend)
                .collect(Collectors.toList());
    }


    public List<User> getFindFriendsOnPage(int page, Long id) {
        this.pageFindFriends = page;
        Pageable pageable = new PageableImplementation(page, this.sizeFindFriendsPage);
        Paginator<User> paginator = new Paginator<>(pageable, findFriends(id));
        return paginator.paginate().getContent().collect(Collectors.toList());
    }

    public List<User> getNextFindFriends(Long id) {
        this.pageFindFriends++;
        List<User> friends = StreamSupport.stream(findFriends(id).spliterator(), false).collect(Collectors.toList());
        if (this.sizeFindFriendsPage >= friends.size()) {
            this.pageFindFriends = 0;
            return getFindFriendsOnPage(0, id);
        }
        if (this.pageFindFriends + sizeFindFriendsPage > friends.size()) {
            this.pageFindFriends--;
        }
        return getFindFriendsOnPage(pageFindFriends, id);
    }

    public List<User> getPreviousFindFriends(Long id) {
        this.pageFindFriends--;
        if (this.pageFindFriends <= 0) {
            this.pageFindFriends = 0;
        }
        return getFindFriendsOnPage(pageFindFriends, id);
    }

    public Iterable<User> getMutualFriends(User user1, User user2) {
        Iterable<User> allFriends = getAll();
        List<User> user1Friends = new ArrayList<>();
        List<User> user2Friends = new ArrayList<>();
        for (User user : allFriends) {
            if (user1.getId().equals(user.getId()))
                user1Friends = user.getFriends();
            else if (user2.getId().equals(user.getId()))
                user2Friends = user.getFriends();
        }

        List<User> mutualFriends = new ArrayList<>();
        for (User user : user1Friends) {
            if (user2Friends.contains(user))
                mutualFriends.add(user);
        }
        return mutualFriends;
    }


    public List<FriendDTO> getNextFriends(Long id) {
        this.pageFriends++;
        List<FriendDTO> friends = user_friendsList(id);
        if (this.sizeFriendsPage >= friends.size()) {
            this.pageFriends = 0;
            return getFriendsOnPage(0, id);
        }
        if (this.pageFriends + this.sizeFriendsPage > friends.size()) {
            this.pageFriends--;
        }
        return getFriendsOnPage(pageFriends, id);
    }

    public List<FriendDTO> getPreviousFriends(Long id) {
        this.pageFriends--;
        if (this.pageFriends <= 0) {
            this.pageFriends = 0;
        }
        return getFriendsOnPage(pageFriends, id);
    }

    public List<FriendDTO> getFriendsOnPage(int page, Long id) {
        this.pageFriends = page;
        Pageable pageable = new PageableImplementation(pageFriends, this.sizeFriendsPage);
        Paginator<FriendDTO> paginator = new Paginator<>(pageable, user_friendsList(id));
        return paginator.paginate().getContent().collect(Collectors.toList());
    }
}

