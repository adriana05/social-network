package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.utils.event.Event;
import socialnetwork.utils.event.EventFunctions;
import socialnetwork.utils.event.NotificationEvent;
import socialnetwork.utils.observers.Observable;
import socialnetwork.utils.observers.Observer;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ServiceNotification implements Observable<Event> {

    private final Repository<Long, Notification> notificationRepository;
    private final PagingRepository<Long, IncomingEvent> eventRepository;
    private final Repository<Tuple<Long, Long>, Participant> participantRepository;
    private List<Observer<Event>> observers = new ArrayList<>();

    public ServiceNotification(Repository<Long, Notification> notificationRepository, PagingRepository<Long, IncomingEvent> eventRepository, Repository<Tuple<Long, Long>, Participant> participantRepository) {
        this.notificationRepository = notificationRepository;
        this.eventRepository = eventRepository;
        this.participantRepository = participantRepository;

    }

    public List<Notification> findAllNotificationsForUser(User user) {
        Iterable<Notification> allNotifications = notificationRepository.findAll();

        return StreamSupport.stream(allNotifications.spliterator(), false)
                .filter(x -> x.getIdUser().equals(user.getId()))
                .sorted(Comparator.comparing(Notification::getDate).reversed())
                .collect(Collectors.toList());
    }

    public void updateAllNotifications(User user) {
        findAllNotificationsForUser(user).forEach(notificationRepository::update);
    }

    public boolean checkForNewNotifications(User user) {
        List<Notification> allNotificationsForAnUser = findAllNotificationsForUser(user);
        for (Notification notification : allNotificationsForAnUser) {
            if (!notification.isOpened()) {
                return true;
            }
        }
        return false;
    }

    public Notification add(Notification notification) {
        return notificationRepository.save(notification);
    }

    public void sendNotification(User user) {
        boolean newNotifications = false;
        List<Notification> notifications = findAllNotificationsForUser(user);

        List<Participant> participants = StreamSupport.stream(participantRepository.findAll().spliterator(), false)
                .filter(x -> x.getNotification().equals(true) && x.getIdUser().equals(user.getId()))
                .collect(Collectors.toList());

        for (Participant participant : participants) {
            IncomingEvent incomingEvent = eventRepository.findOne(participant.getIdEvent());
            long daysUntilEvent = ChronoUnit.DAYS.between(LocalDateTime.now(), incomingEvent.getDate()) + 1;
            if (daysUntilEvent <= 30 && daysUntilEvent >= 0) {

                String text = incomingEvent.getTitle() + " is getting closer, less than " + daysUntilEvent + " days remained.";

                Notification notification = new Notification(user.getId(), text, false, participant.getIdEvent(), LocalDateTime.now());
                if (!notifications.contains(notification)) {
                    notificationRepository.save(notification);
                    newNotifications = true;
                }
            }
        }
        if (newNotifications)
            notifyObservers(new NotificationEvent(EventFunctions.SEND));
    }

    @Override
    public void addObserver(Observer<Event> e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer<Event> e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(Event t) {
        observers.stream().forEach(x -> x.update(t));
    }
}
