package socialnetwork.statistics;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import socialnetwork.domain.FriendDTO;
import socialnetwork.domain.Message;
import socialnetwork.domain.User;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class ActivityLog {

    private final User user;
    private final LocalDate from;
    private final LocalDate to;
    private final String fileName;
    PDPage page;
    PDDocument document;
    PDPageContentStream pageContentStream;

    public ActivityLog(User user, LocalDate from, LocalDate to, String fileName) {
        this.user = user;
        this.from = from;
        this.to = to;
        this.fileName = fileName;

        setUp();
    }

    private void setUp() {
        try {
            document = new PDDocument();
            page = new PDPage();
            document.addPage(page);

            pageContentStream = new PDPageContentStream(document, page);
            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 22);
            pageContentStream.newLineAtOffset(230, 720);
            pageContentStream.showText("Recent Activity");
            pageContentStream.endText();

            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
            pageContentStream.newLineAtOffset(50, 680);
            pageContentStream.showText("First Name: " + user.getFirstName());
            pageContentStream.endText();

            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
            pageContentStream.newLineAtOffset(50, 660);
            pageContentStream.showText("Last Name: " + user.getLastName());
            pageContentStream.endText();

            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
            pageContentStream.newLineAtOffset(50, 640);
            pageContentStream.showText("Activity registered from: " + from.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + ", to: " + to.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            pageContentStream.endText();


            PDImageXObject imageXObject = PDImageXObject.createFromFile("C:\\Users\\adrii\\Desktop\\2020\\MAP\\Socialnetwork\\src\\main\\resources\\images\\deer7.png", document);
            imageXObject.setHeight(200);
            imageXObject.setWidth(200);
            pageContentStream.drawImage(imageXObject, 400, 600);

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    public void createPdf(List<Message> messageList, List<FriendDTO> friendshipList, User specificUser) throws IOException {
        String type;
        if (messageList == null) {
            type = "List of friendships";
        } else if (friendshipList == null) {
            if (specificUser == null)
                type = "List of messages";
            else
                type = "List of messages received from " + specificUser.getFirstName() + " " + specificUser.getLastName();
        } else {
            type = "All messages and friendships received";
        }

        try {
            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
            pageContentStream.newLineAtOffset(50, 620);
            pageContentStream.showText("Type: " + type);
            pageContentStream.endText();

            if (messageList == null || messageList.size() == 0) {
                insertFriendships(friendshipList);
            } else if (friendshipList == null || friendshipList.size() == 0) {
                if (specificUser != null)
                    insertMessagesFromASpecificUser(messageList, specificUser);
                else {
                    insertMessages(messageList);
                }
            } else {
                insertMessagesFriendships(messageList, friendshipList);
            }

            pageContentStream.close();
            document.save("C:\\Users\\adrii\\Desktop\\" + fileName + ".pdf");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private int checkEndOfPage(int y) throws IOException {
        if (y - 40 <= 0) {
            y = 750;
            PDPage newPage = new PDPage();
            document.addPage(newPage);
            try {
                pageContentStream.close();
                pageContentStream = new PDPageContentStream(document, newPage);
            } catch (IOException ioException) {
                throw new IOException();
            }
        }
        return y;
    }

    private void insertMessagesFriendships(List<Message> messageList, List<FriendDTO> friendshipList) throws IOException {
        int y = 540;
        try {
            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 16);
            pageContentStream.newLineAtOffset(50, 570);
            pageContentStream.showText("Message and Friendship List");
            pageContentStream.endText();
        } catch (IOException ioException) {
            throw new IOException();
        }
        LocalDate currentDate;
        if (messageList.get(0).getDate().toLocalDate().isBefore(friendshipList.get(0).getDate().toLocalDate())) {
            currentDate = messageList.get(0).getDate().toLocalDate();
        } else {
            currentDate = friendshipList.get(0).getDate().toLocalDate();
        }
        try {
            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 14);
            pageContentStream.newLineAtOffset(50, y);
            pageContentStream.showText(currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            pageContentStream.endText();
            y -= 20;
        } catch (IOException ioException) {
            throw new IOException();
        }
        int messageIndex = 0, friendshipIndex = 0;

        while (messageIndex < messageList.size() && friendshipIndex < friendshipList.size()) {
            y -= 20;
            y = checkEndOfPage(y);

            Message message = messageList.get(messageIndex);
            FriendDTO friendship = friendshipList.get(friendshipIndex);
            if (message.getDate().isBefore(friendship.getDate())) {

                if (message.getDate().toLocalDate().isAfter(currentDate)) {
                    currentDate = message.getDate().toLocalDate();
                    pageContentStream.beginText();
                    pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 14);
                    pageContentStream.newLineAtOffset(50, y);
                    pageContentStream.showText(currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                    pageContentStream.endText();
                    y -= 20;
                }

                pageContentStream.beginText();
                pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
                pageContentStream.newLineAtOffset(50, y);
                pageContentStream.showText("Message from: " + message.getFrom().getFirstName() + " " + message.getFrom().getLastName() + " at " + message.getDate().getHour() + ":" + message.getDate().getSecond());
                pageContentStream.endText();
                y -= 20;

                pageContentStream.beginText();
                pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
                pageContentStream.newLineAtOffset(50, y);
                pageContentStream.showText(message.getMessage().replace("\n", "").replace("\r", ""));
                pageContentStream.endText();

                messageIndex += 1;
            } else {
                if (friendship.getDate().toLocalDate().isAfter(currentDate)) {
                    currentDate = friendship.getDate().toLocalDate();
                    pageContentStream.beginText();
                    pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 14);
                    pageContentStream.newLineAtOffset(50, y);
                    pageContentStream.showText(currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                    pageContentStream.endText();
                    y -= 20;
                }

                pageContentStream.beginText();
                pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
                pageContentStream.newLineAtOffset(50, y);
                pageContentStream.showText("You and " + friendship.getFirstName() + " " + friendship.getLastName() + " became friends at " + friendship.getDate().getHour() + ":" + friendship.getDate().getSecond());
                pageContentStream.newLineAtOffset(50, y);
                pageContentStream.endText();

                friendshipIndex += 1;
            }
            y -= 20;
        }

        while (messageIndex < messageList.size()) {
            y = checkEndOfPage(y);

            Message message = messageList.get(messageIndex);
            if (message.getDate().toLocalDate().isAfter(currentDate)) {
                currentDate = message.getDate().toLocalDate();
                pageContentStream.beginText();
                pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 14);
                pageContentStream.newLineAtOffset(50, y);
                pageContentStream.showText(currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                pageContentStream.endText();
                y -= 20;
            }

            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
            pageContentStream.newLineAtOffset(50, y);
            pageContentStream.showText("Message from: " + message.getFrom().getFirstName() + " " + message.getFrom().getLastName() + " at " + message.getDate().getHour() + ":" + message.getDate().getSecond());
            pageContentStream.endText();
            y -= 20;

            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
            pageContentStream.newLineAtOffset(50, y);
            pageContentStream.showText(message.getMessage().replace("\n", "").replace("\r", ""));
            pageContentStream.endText();
            y -= 20;

            messageIndex += 1;
        }

        while (friendshipIndex < friendshipList.size()) {
            y = checkEndOfPage(y);

            FriendDTO friendship = friendshipList.get(friendshipIndex);
            if (friendship.getDate().toLocalDate().isAfter(currentDate)) {
                currentDate = friendship.getDate().toLocalDate();
                pageContentStream.beginText();
                pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 14);
                pageContentStream.newLineAtOffset(50, y);
                pageContentStream.showText(currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                pageContentStream.endText();
                y -= 20;
            }

            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
            pageContentStream.newLineAtOffset(50, y);
            pageContentStream.showText("You and " + friendship.getFirstName() + " " + friendship.getLastName() + " became friends at " + friendship.getDate().getHour() + ":" + friendship.getDate().getSecond());
            pageContentStream.newLineAtOffset(50, y);
            pageContentStream.endText();

            friendshipIndex += 1;
            y -= 20;
        }
    }

    private void insertMessages(List<Message> messageList) throws IOException {
        int y = 540;
        try {
            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 16);
            pageContentStream.newLineAtOffset(50, 570);
            pageContentStream.showText("Message List");
            pageContentStream.endText();
        } catch (IOException ioException) {
            throw new IOException();
        }
        LocalDate currentDate = messageList.get(0).getDate().toLocalDate();
        try {
            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 14);
            pageContentStream.newLineAtOffset(50, y);
            pageContentStream.showText(currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            pageContentStream.endText();
            y -= 20;
        } catch (IOException ioException) {
            throw new IOException();
        }
        for (Message message : messageList) {
            try {
                y = checkEndOfPage(y);

                if (message.getDate().toLocalDate().isAfter(currentDate)) {
                    currentDate = message.getDate().toLocalDate();
                    pageContentStream.beginText();
                    pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 14);
                    pageContentStream.newLineAtOffset(50, y);
                    pageContentStream.showText(currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                    pageContentStream.endText();
                    y -= 20;
                }

                pageContentStream.beginText();
                pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
                pageContentStream.newLineAtOffset(50, y);
                pageContentStream.showText("Message from: " + message.getFrom().getFirstName() + " " + message.getFrom().getLastName() + " at " + message.getDate().getHour() + ":" + message.getDate().getSecond());
                pageContentStream.endText();

                y -= 20;

                pageContentStream.beginText();
                pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
                pageContentStream.newLineAtOffset(50, y);
                pageContentStream.showText(message.getMessage().replace("\n", "").replace("\r", ""));
                pageContentStream.endText();
                y -= 40;

            } catch (IOException ioException1) {
                throw new IOException();
            }
        }
    }


    private void insertMessagesFromASpecificUser(List<Message> messageList, User specificUser) throws IOException {
        int y = 540;
        try {
            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 16);
            pageContentStream.newLineAtOffset(50, 570);
            pageContentStream.showText("Message List");
            pageContentStream.endText();
        } catch (IOException ioException) {
            throw new IOException();
        }
        LocalDate currentDate = messageList.get(0).getDate().toLocalDate();
        try {
            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 14);
            pageContentStream.newLineAtOffset(50, y);
            pageContentStream.showText(currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            pageContentStream.endText();
            y -= 20;
        } catch (IOException ioException) {
            throw new IOException();
        }
        for (Message message : messageList) {
            try {
                y = checkEndOfPage(y);

                if (message.getDate().toLocalDate().isAfter(currentDate)) {
                    currentDate = message.getDate().toLocalDate();
                    pageContentStream.beginText();
                    pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 14);
                    pageContentStream.newLineAtOffset(50, y);
                    pageContentStream.showText(currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                    pageContentStream.endText();
                    y -= 20;
                }

                pageContentStream.beginText();
                pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
                pageContentStream.newLineAtOffset(50, y);
                pageContentStream.showText("Message sent at " + message.getDate().getHour() + ":" + message.getDate().getSecond());
                pageContentStream.endText();

                y -= 20;

                pageContentStream.beginText();
                pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
                pageContentStream.newLineAtOffset(50, y);
                pageContentStream.showText(message.getMessage().replace("\n", "").replace("\r", ""));
                pageContentStream.endText();
                y -= 40;

            } catch (IOException ioException1) {
                throw new IOException();
            }
        }
    }

    private void insertFriendships(List<FriendDTO> friendshipList) throws IOException {
        int y = 540;
        try {
            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 16);
            pageContentStream.newLineAtOffset(50, 570);
            pageContentStream.showText("Friendship List");
            pageContentStream.endText();
        } catch (IOException ioException) {
            throw new IOException();
        }
        LocalDate currentDate = friendshipList.get(0).getDate().toLocalDate();
        try {
            pageContentStream.beginText();
            pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 14);
            pageContentStream.newLineAtOffset(50, y);
            pageContentStream.showText(currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            pageContentStream.endText();
            y -= 20;
        } catch (IOException ioException) {
            throw new IOException();
        }
        for (FriendDTO friendship : friendshipList) {
            try {
                y = checkEndOfPage(y);

                if (friendship.getDate().toLocalDate().isAfter(currentDate)) {
                    currentDate = friendship.getDate().toLocalDate();
                    y -= 20;
                    pageContentStream.beginText();
                    pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 14);
                    pageContentStream.newLineAtOffset(50, y);
                    pageContentStream.showText(currentDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
                    pageContentStream.endText();
                    y -= 20;
                }

                pageContentStream.beginText();
                pageContentStream.setFont(PDType1Font.TIMES_ROMAN, 12);
                pageContentStream.newLineAtOffset(50, y);
                pageContentStream.showText("You and " + friendship.getFirstName() + " " + friendship.getLastName() + " became friends at " + friendship.getDate().getHour() + ":" + friendship.getDate().getSecond());
                pageContentStream.newLineAtOffset(50, y);
                pageContentStream.endText();

                y -= 20;

            } catch (IOException ioException1) {
                throw new IOException();
            }
        }
    }
}


