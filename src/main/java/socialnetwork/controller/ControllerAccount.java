package socialnetwork.controller;

import javafx.animation.TranslateTransition;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import socialnetwork.domain.PageDTO;
import socialnetwork.domain.User;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.exceptions.RepositoryExceptions;
import socialnetwork.service.*;

import java.io.IOException;

public class ControllerAccount {

    public Label logInLabelTextField;
    public Label signUpLabelTextField;
    public Button singUpVBoxBtn;
    public VBox VBoxSignUp;
    public VBox VBoxSignIn;
    public TextField emailSignIn;
    public PasswordField passwordSignIn;
    public Button singInVBoxBtn;
    public TextField firstNameSignUp;
    public TextField lastNameSignUp;
    public TextField emailSignUp;
    public PasswordField passwordSignUp;

    ServiceUser serviceUser;
    ServiceFriendRequest serviceFriendRequest;
    ServiceMessage serviceMessage;
    ServiceFriendship serviceFriendship;
    ServiceEvent serviceEvent;
    ServiceNotification serviceNotification;
    Stage stage;


    public void setUp(ServiceUser userService, ServiceFriendRequest serviceFriendRequest, ServiceMessage serviceMessage, ServiceFriendship serviceFriendship, ServiceEvent serviceEvent, ServiceNotification serviceNotification, Stage stage) {
        this.serviceUser = userService;
        this.serviceFriendRequest = serviceFriendRequest;
        this.serviceMessage = serviceMessage;
        this.serviceFriendship = serviceFriendship;
        this.serviceEvent = serviceEvent;
        this.serviceNotification = serviceNotification;

        this.stage = stage;
        stage.getIcons().add(new Image("/images/deerIcon1.png"));

    }


    @FXML
    public void initialize() {
        TranslateTransition translateTransition = new TranslateTransition(Duration.seconds(1), VBoxSignIn);
        translateTransition.setToX(170);
        translateTransition.play();

        TranslateTransition translateTransition1 = new TranslateTransition(Duration.seconds(1), VBoxSignUp);
        translateTransition1.setToX(170);
        translateTransition1.play();


    }

    @FXML
    private void handleSignInBtnOnAnchorPane(ActionEvent event) {
        TranslateTransition translateTransition = new TranslateTransition(Duration.seconds(1), VBoxSignIn);
        translateTransition.setToX(170);
        translateTransition.play();

        TranslateTransition translateTransition1 = new TranslateTransition(Duration.seconds(1), VBoxSignUp);
        translateTransition1.setToX(170);
        translateTransition1.play();

        emailSignIn.setText("");
        passwordSignIn.setText("");
        logInLabelTextField.setText("");

        VBoxSignIn.setVisible(true);
        VBoxSignUp.setVisible(false);


    }

    @FXML
    private void handleSignUpBtnOnAnchorPane(ActionEvent event) {

        TranslateTransition translateTransition = new TranslateTransition(Duration.seconds(1), VBoxSignIn);
        translateTransition.setToX(-200);
        translateTransition.play();

        TranslateTransition translateTransition1 = new TranslateTransition(Duration.seconds(1), VBoxSignUp);
        translateTransition1.setToX(-200);
        translateTransition1.play();

        firstNameSignUp.setText("");
        lastNameSignUp.setText("");
        emailSignUp.setText("");
        passwordSignUp.setText("");
        signUpLabelTextField.setText("");

        VBoxSignIn.setVisible(false);
        VBoxSignUp.setVisible(true);

    }

    @FXML
    public void handleNewSignUp(ActionEvent event) throws IOException {
        String firstName = firstNameSignUp.getText();
        String lastName = lastNameSignUp.getText();
        String email = emailSignUp.getText();
        String password = passwordSignUp.getText();

        try {
            User user = new User(firstName, lastName, email, password);
            serviceUser.add(user);
            signUpLabelTextField.setText("Success!");
            firstNameSignUp.setText("");
            lastNameSignUp.setText("");
            emailSignUp.setText("");
            passwordSignUp.setText("");

        } catch (ValidationException | RepositoryExceptions exception) {
            signUpLabelTextField.setText(exception.getMessage());
        }
    }

    @FXML
    private void handleSignIn(ActionEvent event) throws IOException {
        logInLabelTextField.setText("");
        String email = emailSignIn.getText();
        String password = passwordSignIn.getText();

        User user = serviceUser.findOneByEmail(email, password);
        if (user == null) {
            logInLabelTextField.setText("Invalid email or password");
            return;
        }

        FXMLLoader profileLoader = new FXMLLoader();
        profileLoader.setLocation(getClass().getResource("/views/profile.fxml"));
        AnchorPane applicationLayout = profileLoader.load();

        Scene scene = new Scene(applicationLayout);
        Stage primaryStage = new Stage();
        primaryStage.getIcons().add(new Image("/images/deerIcon1.png"));
        primaryStage.setScene(scene);
        primaryStage.initStyle(StageStyle.UNDECORATED);

        PageDTO pageDTO = new PageDTO(user.getFirstName(), user.getLastName(),
                serviceUser.getFriendsOnPage(0, user.getId()),
                serviceFriendRequest.getFriendRequestsOnPage(0, user.getId()),
                serviceUser.getFindFriendsOnPage(0, user.getId()),
                serviceFriendRequest.getSentFriendRequestsOnPage(0, user.getId()),
                serviceMessage.getReceivedUsersMessages(user),
                serviceEvent.getEventsOnPage(0));

        ControllerProfile profileController = profileLoader.getController();
        profileController.setUp(pageDTO, serviceUser, serviceFriendRequest, serviceMessage, serviceFriendship, serviceEvent, serviceNotification, primaryStage, user);
        stage.close();
        primaryStage.show();


    }

    public void handleEneterkeyLoginCredentials(KeyEvent keyEvent) throws IOException {
        if (keyEvent.getCode().equals(KeyCode.ENTER))
            handleSignIn(null);
    }

}
