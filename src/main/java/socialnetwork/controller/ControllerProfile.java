package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import socialnetwork.domain.*;
import socialnetwork.service.*;
import socialnetwork.service.exceptions.ServiceExceptions;
import socialnetwork.statistics.ActivityLog;
import socialnetwork.utils.NotifyTask;
import socialnetwork.utils.event.Event;
import socialnetwork.utils.event.EventType;
import socialnetwork.utils.observers.Observer;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class ControllerProfile implements Observer<Event> {

    public Button findFriendsBtn;
    public Label userLoggedInNameField;
    public ListView<FriendDTO> listFriends;
    public AnchorPane findFriendsAnchorPane;
    public ListView<User> listFindFriends;
    public ListView<User> listFriendRequest;
    public Button sendFriendRequest;
    public Button cancelFriendRequest;
    public TableView<UserFriendRequestDto> tableFriendRequest;
    public Button acceptFriendRequest;
    public Button rejectFriendRequest;
    public TableColumn<UserFriendRequestDto, String> columnName;
    public TableColumn<UserFriendRequestDto, LocalDateTime> columnDate;
    public TableColumn<UserFriendRequestDto, String> columnStatus;
    public AnchorPane mainProfile;
    public Hyperlink backHyperlink;
    public TextField searchNewFriends;
    public ImageView logOutIcon;
    public AnchorPane newFriendProfile;
    public Label friendNameLabelChat;
    public ListView<User> listMutualFriends;
    public ImageView exitFriendProfile;
    public Label mutualFriendsLabel;
    public AnchorPane friendRequestProfile;
    public Label mutualFriendsLabel1;
    public Label friendNameTextFieldOnFriendRequestProfilePane;
    public ListView<User> listMutualFriendsOnFriendRequestPane;
    public ImageView exitFriendProfile1;
    public TableView<Message> tableMessages;
    public TextArea textAreaChat;
    public TableColumn<Message, String> columnMessages;
    public Label chatHeader;
    public VBox vBoxChat;
    public ScrollPane scrollPaneMessages;
    public TextField searchFriendRequests;
    public Button btnGroups;
    public Button btnFriendsList;
    public AnchorPane paneFriendProfile;
    public ListView<User> listMutualFriendListOnFriendProfile;
    public Label friendNameTextField;
    public Button btnReplyToOneUser;
    public Label labelTextFieldGroupOrFriendsList;
    public Label mutualFriendsLabelFromFriendListProfile;
    public Button btnRemoveFriendFromFriendList;
    public Button btnChat;
    public ImageView sendMessageButton;
    public Button btnCreateGroup;
    public AnchorPane anchorPaneChat;
    public TextField searchFriendList;
    public ListView<FriendDTO> listChosenGroupChat;
    public Button btnCreateGroupChat;
    public AnchorPane anchorPaneFriendsList;
    public AnchorPane anchorPaneGroupMessages;
    public AnchorPane anchorPaneGroupChat;
    public TextField searchFriendsGroups;
    public AnchorPane anchorPaneGroupChatList;
    public ListView<FriendDTO> listFriendsToChoseForGroupChat;
    public Button btnActivityLog;
    public AnchorPane anchorPaneActivityLog;
    public DatePicker toDatePickerActivityLog;
    public ComboBox<FriendDTO> chooseFriendComboBox;
    public DatePicker fromDatePickerActivityLog;
    public RadioButton messagesRadioButton;
    public RadioButton friendshipsRadioButton;
    public RadioButton specificFriendRadioButton;
    public ScrollPane scrollPaneActivityLog;
    public VBox vBoxActivityLog;
    public Button generatePdfButton;
    public Button generateActivityLogButton;
    public Hyperlink backHyperlinkActivityLog;
    public Label labelFailGenerateActivityLog;
    public TextField fileNameTextField;
    public AnchorPane eventsAnchorPane;
    public ListView<IncomingEvent> listEvents;
    public TextField locationTextField;
    public TextArea descriptionTextArea;
    public DatePicker eventDatePicker;
    public TextField titleTextField;
    public TextField hourTextField;
    public ListView<User> listPeopleAttendingEvent;
    public Label errorCreateEventLabel;
    public Button subscribeBtn;
    public Button remindMeBtn;
    public ImageView notificationIcon;
    public Button notificationBtn;
    public ListView<Notification> listNotifications;

    ServiceUser serviceUser;
    ServiceFriendRequest serviceFriendRequest;
    ServiceMessage serviceMessage;
    ServiceFriendship serviceFriendship;
    ServiceEvent serviceEvent;
    ServiceNotification serviceNotification;
    User userLoggedIn;
    List<User> sendTo;
    List<FriendDTO> friendsChosenForGroupChat;
    PageDTO pageDTO;
    Long isReplyID;
    NotifyTask notifyTask;
    IncomingEvent scrolledEvent = null;

    ObservableList<FriendDTO> modelFriendsList = FXCollections.observableArrayList();
    ObservableList<UserFriendRequestDto> modelFriendRequests = FXCollections.observableArrayList();
    ObservableList<User> modelFindFriends = FXCollections.observableArrayList();
    ObservableList<User> modelSentFriendRequest = FXCollections.observableArrayList();
    ObservableList<User> modelMutualFriendsOnFindFriendsProfileList = FXCollections.observableArrayList();
    ObservableList<User> modelMutualFriendsOnFriendRequestProfilePane = FXCollections.observableArrayList();
    ObservableList<Message> modelMessageList = FXCollections.observableArrayList();
    ObservableList<User> modelMutualFriendsOnFriendProfilePane = FXCollections.observableArrayList();
    ObservableList<FriendDTO> modelFriendsForGroupChat = FXCollections.observableArrayList();
    ObservableList<FriendDTO> modelFriendsChosenForGroupChat = FXCollections.observableArrayList();
    ObservableList<IncomingEvent> modelIncomingEvents = FXCollections.observableArrayList();
    ObservableList<User> modelAttendingUsers = FXCollections.observableArrayList();
    ObservableList<Notification> modelNotifications = FXCollections.observableArrayList();
    FriendDTO friend = null;
    UserFriendRequestDto userFriendRequestDto = null;
    User findFriend = null;
    User sentriendRequest = null;
    private Stage stage;

    public void setUp(PageDTO pageDTO, ServiceUser userService, ServiceFriendRequest serviceFriendRequest, ServiceMessage serviceMessage, ServiceFriendship serviceFriendship, ServiceEvent serviceEvent, ServiceNotification serviceNotification, Stage stage, User user) {
        this.serviceUser = userService;
        this.serviceFriendRequest = serviceFriendRequest;
        this.serviceMessage = serviceMessage;
        this.serviceFriendship = serviceFriendship;
        this.serviceEvent = serviceEvent;
        this.serviceNotification = serviceNotification;
        this.stage = stage;
        userLoggedIn = user;
        this.pageDTO = pageDTO;
        userLoggedInNameField.setText(pageDTO.getFirstName() + " " + pageDTO.getLastName());

        serviceFriendRequest.addObserver(this);
        serviceMessage.addObserver(this);
        serviceFriendship.addObserver(this);
        serviceEvent.addObserver(this);
        serviceNotification.addObserver(this);

        scrollPaneMessages.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        scrollPaneActivityLog.setHbarPolicy(ScrollPane.ScrollBarPolicy.NEVER);
        friendsChosenForGroupChat = new ArrayList<>();
        chooseFriendComboBox.setVisibleRowCount(5);
        fromDatePickerActivityLog.setEditable(false);
        toDatePickerActivityLog.setEditable(false);
        textAreaChat.setAccessibleText("");
        descriptionTextArea.setWrapText(true);

        notifyTask = new NotifyTask(serviceNotification, user);

        initModel();
    }

    @FXML
    public void initialize() {
        listFriends.setItems(modelFriendsList);
        listFriendRequest.setItems(modelSentFriendRequest);
        listFindFriends.setItems(modelFindFriends);
        listMutualFriends.setItems(modelMutualFriendsOnFindFriendsProfileList);
        listMutualFriendsOnFriendRequestPane.setItems(modelMutualFriendsOnFriendRequestProfilePane);
        listMutualFriendListOnFriendProfile.setItems(modelMutualFriendsOnFriendProfilePane);
        listFriendsToChoseForGroupChat.setItems(modelFriendsForGroupChat);
        listEvents.setItems(modelIncomingEvents);
        listPeopleAttendingEvent.setItems(modelAttendingUsers);
        listNotifications.setItems(modelNotifications);

        chooseFriendComboBox.setItems(modelFriendsList);

        columnName.setCellValueFactory(new PropertyValueFactory<>("fullName"));
        columnDate.setCellValueFactory(new PropertyValueFactory<>("dateString"));
        columnStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
        tableFriendRequest.setItems(modelFriendRequests);

        columnMessages.setCellValueFactory(new PropertyValueFactory<>("fullMessage"));
        tableMessages.setItems(modelMessageList);

        searchNewFriends.textProperty().addListener(x -> handleSearchBarNewFriends(null));
        searchFriendRequests.textProperty().addListener(x -> handleSearchBarFriendRequest(null));
        searchFriendList.textProperty().addListener(x -> handleSearchFriendsInFriendsList(null));
        searchFriendsGroups.textProperty().addListener(x -> handleSearchFriendsForGroupChat(null));

        scrollPaneMessages.setVvalue(1.0);
    }

    private void initModel() {
        List<FriendDTO> friendsList = new ArrayList<>(pageDTO.getFriendsList());
        modelFriendsList.setAll(friendsList);
        modelFriendsForGroupChat.setAll(friendsList);

        List<UserFriendRequestDto> friendRequests1 = StreamSupport.stream(pageDTO.getFriendRequestsList().spliterator(), false).collect(Collectors.toList());
        modelFriendRequests.setAll(friendRequests1);

        List<User> findFriends = StreamSupport.stream(pageDTO.getUnknownUsersList().spliterator(), false).collect(Collectors.toList());
        modelFindFriends.setAll(findFriends);

        List<User> sentFriendRequests = StreamSupport.stream(pageDTO.getFriendRequestsSentList().spliterator(), false).collect(Collectors.toList());
        modelSentFriendRequest.setAll(sentFriendRequests);

        List<Message> messageList = new ArrayList<>(pageDTO.getMessageList());
        modelMessageList.setAll(messageList);

        List<IncomingEvent> eventList = new ArrayList<>(pageDTO.getEventList());
        modelIncomingEvents.setAll(eventList);

        List<Notification> getAllNotifications = new ArrayList<>(serviceNotification.findAllNotificationsForUser(userLoggedIn));
        modelNotifications.setAll(getAllNotifications);

        if (serviceNotification.checkForNewNotifications(userLoggedIn)) {
            notificationIcon.setStyle("-fx-image: url('/images/notificationRed.png')");
        }
    }

    @Override
    public void update(Event event) {

        if (event.getEvent() == EventType.FRIEND_REQUEST_EVENT || event.getEvent() == EventType.FRIENDSHIP_EVENT) {
            Iterable<FriendDTO> friends = serviceUser.getFriendsOnPage(0, userLoggedIn.getId());
            List<FriendDTO> friendsList = StreamSupport.stream(friends.spliterator(), false).collect(Collectors.toList());
            modelFriendsList.setAll(friendsList);
            modelFriendsForGroupChat.setAll(friendsList);

            Iterable<User> iterableFindFriends = serviceUser.getFindFriendsOnPage(0, userLoggedIn.getId());
            List<User> findFriends = StreamSupport.stream(iterableFindFriends.spliterator(), false).collect(Collectors.toList());
            modelFindFriends.setAll(findFriends);

            Iterable<UserFriendRequestDto> friendRequests = serviceFriendRequest.getFriendRequestsOnPage(0, userLoggedIn.getId());
            List<UserFriendRequestDto> friendRequests1 = StreamSupport.stream(friendRequests.spliterator(), false).collect(Collectors.toList());
            modelFriendRequests.setAll(friendRequests1);

            Iterable<User> iterableSentFriendRequests = serviceFriendRequest.getSentFriendRequestsOnPage(0, userLoggedIn.getId());
            List<User> sentFriendRequests = StreamSupport.stream(iterableSentFriendRequests.spliterator(), false).collect(Collectors.toList());
            modelSentFriendRequest.setAll(sentFriendRequests);

        } else if (event.getEvent() == EventType.MESSAGE_EVENT) {
//            if (sendTo != null)
//                setUpChat(sendTo.size() != 1);

            Iterable<Message> messages = serviceMessage.getReceivedUsersMessages(userLoggedIn);
            List<Message> messageList = StreamSupport.stream(messages.spliterator(), false).collect(Collectors.toList());
            modelMessageList.setAll(messageList);

        } else if (event.getEvent() == EventType.INCOMING_EVENT) {

            List<IncomingEvent> eventList = serviceEvent.getEventsOnPage(0);
            modelIncomingEvents.setAll(eventList);
        } else if (event.getEvent() == EventType.NOTIFICATION) {

            List<Notification> getAllNotifications = new ArrayList<>(serviceNotification.findAllNotificationsForUser(userLoggedIn));
            modelNotifications.setAll(getAllNotifications);

            notificationIcon.setStyle("-fx-image: url('/images/notificationRed.png')");
        }
    }

    private void setUpChat(boolean isGroup) {
        Iterable<Message> chatMessages;
        if (isGroup) {
            chatMessages = serviceMessage.getGroupConversation(sendTo, userLoggedIn);
        } else {
            chatMessages = serviceMessage.conversation(userLoggedIn.getId(), sendTo.get(0).getId());
        }

        vBoxChat.getChildren().removeAll(vBoxChat.getChildren());
        chatMessages.forEach(message -> {
            Pane pane = new Pane();
            Text text = new Text(message.getMessage());
            pane.layout();

            double width = text.getLayoutBounds().getWidth();
            double padding = 40;
            TextArea textArea = new TextArea();

            if (!message.getFrom().equals(userLoggedIn)) {
                textArea.setAccessibleText(message.getId().toString());
                textArea.setOnMouseClicked(x -> handleReplyOneUser(textArea.getText(), textArea.getAccessibleText()));
            }
            int rowCount = (int) ((width + padding) / 180);
            double rowWidth = width + padding;
            textArea.setPrefRowCount(rowCount);
            textArea.setText(message.getMessage());
            textArea.setWrapText(true);
            textArea.setEditable(false);

            if (message.getFrom().getId().equals(userLoggedIn.getId())) {
                if (rowWidth > 180)
                    rowWidth = 180;
                rowCount = (int) (rowWidth / 180);
                textArea.setPrefRowCount(rowCount);

                textArea.setMaxWidth(rowWidth);
                textArea.setLayoutX(210 - rowWidth);

                if (message.getIsReply() != 0) {
                    Message originalMessage = serviceMessage.findOne(message.getIsReply());
                    Text replyMessage = new Text();
                    replyMessage.setText("Replied to: " + originalMessage.getMessage());
                    Label replyLabel = new Label("Replied to: " + originalMessage.getMessage());

                    double widthReply = replyMessage.getLayoutBounds().getWidth();
                    double rowWidthReply = widthReply + 0;
                    replyLabel.setWrapText(true);

                    if (rowWidthReply > 180) {
                        rowWidthReply = 180;
                    }
                    double rowCountReply = (int) (rowWidthReply / 180 + 1);
                    replyLabel.setMaxHeight(25 * rowCountReply);
                    replyLabel.setMaxWidth(rowWidthReply);
                    replyLabel.setLayoutX(210 - rowWidthReply);
                    textArea.setLayoutY(25 * rowCountReply);
                    replyLabel.setAlignment(Pos.BOTTOM_RIGHT);

                    pane.getChildren().add(replyLabel);
                }

            } else {
                Label label = new Label();
                label.setText(message.getFrom().getFirstName() + " " + message.getFrom().getLastName());
                label.setPrefWidth(150);
                label.setPrefHeight(20);
                label.setMinHeight(25);
                pane.getChildren().add(label);

                if (rowWidth > 180)
                    rowWidth = 180;
                rowCount = (int) (rowWidth / 180);
                textArea.setPrefRowCount(rowCount);
                textArea.setMaxWidth(rowWidth);

                double rowCountReply = 0;
                if (message.getIsReply() != 0) {
                    Message originalMessage = serviceMessage.findOne(message.getIsReply());
                    Text replyMessage = new Text();
                    replyMessage.setText("Replied to: " + originalMessage.getMessage());
                    Label replyLabel = new Label("Replied to: " + originalMessage.getMessage());
                    double widthReply = replyMessage.getLayoutBounds().getWidth();
                    double rowWidthReply = widthReply + 40;
                    replyLabel.setWrapText(true);
                    if (rowWidthReply > 180) {
                        rowWidthReply = 180;
                    }
                    rowCountReply = (int) (rowWidthReply / 180 + 1);
                    replyLabel.setMaxHeight(25 * rowCountReply);
                    replyLabel.setMaxWidth(rowWidthReply);
                    replyLabel.setLayoutX(10);
                    replyLabel.setLayoutY(20);
                    pane.getChildren().add(replyLabel);
                }

                textArea.setLayoutY(25 * rowCountReply + 20);
                label.setLayoutX(10);
                textArea.setLayoutX(10);
            }

            pane.getChildren().add(textArea);
            vBoxChat.getChildren().add(pane);
        });
        scrollPaneMessages.setVvalue(1.0);

    }

    private void handleReplyOneUser(String text, String accessibleText) {
        Message message = serviceMessage.findOne(Long.parseLong(accessibleText));
        sendTo = new ArrayList<>();
        sendTo.add(message.getFrom());

        chatHeader.setText(message.getFrom().getFirstName() + " " + message.getFrom().getLastName());
        scrollPaneMessages.setVvalue(1.0);
        isReplyID = message.getId();
        textAreaChat.setAccessibleText(text);

        setUpChat(false);
    }

    public void handleOpenFindFriends(ActionEvent event) {
        findFriendsAnchorPane.setVisible(true);

    }

    public void handleSendFriendRequest(ActionEvent event) {
        User selected = (User) listFindFriends.getSelectionModel().getSelectedItem();
        if (selected != null) {
            try {
                serviceFriendRequest.add(userLoggedIn, selected);
            } catch (ServiceExceptions serviceExceptions) {
                MessageAlert.showErrorMessage(null, serviceExceptions.getMessage());

            }
        }
        newFriendProfile.setVisible(false);
    }

    public void handleCancelFriendRequest(ActionEvent event) {
        User selected = (User) listFriendRequest.getSelectionModel().getSelectedItem();
        if (selected != null) {
            serviceFriendRequest.cancelFriendRequest(userLoggedIn, selected);
        }
        friendRequestProfile.setVisible(false);
    }

    public void handleAcceptFriendRequest(ActionEvent event) {
        UserFriendRequestDto selected = (UserFriendRequestDto) tableFriendRequest.getSelectionModel().getSelectedItem();
        if (selected != null) {
            FriendRequest friendRequest = serviceFriendRequest.findOne(selected.getId());
            if (friendRequest.getStatus().equals("pending"))
                serviceFriendRequest.acceptFriendRequest(friendRequest);
        }
    }

    public void handleRejectFriendRequest(ActionEvent event) {
        UserFriendRequestDto selected = (UserFriendRequestDto) tableFriendRequest.getSelectionModel().getSelectedItem();
        if (selected != null) {
            FriendRequest friendRequest = serviceFriendRequest.findOne(selected.getId());
            if (friendRequest.getStatus().equals("pending"))
                serviceFriendRequest.rejectFriendRequest(friendRequest);
        }

    }

    public void handleBackHyperlink(ActionEvent event) {
        findFriendsAnchorPane.setVisible(false);
    }

    public void handleSearchBarNewFriends(ActionEvent event) {
        Iterable<User> iterableFindFriends = serviceUser.findFriends(userLoggedIn.getId());
        List<User> findFriends = StreamSupport.stream(iterableFindFriends.spliterator(), false).collect(Collectors.toList());
        modelFindFriends.setAll(findFriends.stream()
                .filter(user -> user.toString().toLowerCase().contains(searchNewFriends.getText().toLowerCase()))
                .collect(Collectors.toList()));
    }

    public void handleSearchBarFriendRequest(ActionEvent event) {
        Iterable<User> iterableSentFriendRequests = serviceFriendRequest.getSentFriendRequests(userLoggedIn.getId());
        List<User> sentFriendRequests = StreamSupport.stream(iterableSentFriendRequests.spliterator(), false).collect(Collectors.toList());
        modelSentFriendRequest.setAll(sentFriendRequests.stream()
                .filter(user -> user.toString().toLowerCase().contains(searchFriendRequests.getText().toLowerCase()))
                .collect(Collectors.toList()));
    }

    public void handleSearchFriendsInFriendsList(ActionEvent event) {
        Iterable<FriendDTO> friends = serviceUser.user_friendsList(userLoggedIn.getId());
        List<FriendDTO> friendsList = StreamSupport.stream(friends.spliterator(), false).collect(Collectors.toList());
        modelFriendsList.setAll(friendsList.stream()
                .filter(user -> user.toString().toLowerCase().contains(searchFriendList.getText().toLowerCase()))
                .collect(Collectors.toList()));
    }

    public void handleSearchFriendsForGroupChat(ActionEvent event) {
        Iterable<FriendDTO> friends = serviceUser.user_friendsList(userLoggedIn.getId());
        List<FriendDTO> friendsList = StreamSupport.stream(friends.spliterator(), false).collect(Collectors.toList());
        modelFriendsForGroupChat.setAll(friendsList.stream()
                .filter(user -> user.toString().toLowerCase().contains(searchFriendsGroups.getText().toLowerCase()))
                .collect(Collectors.toList()));
    }

    public void handleLogOut(MouseEvent mouseEvent) throws IOException {
        stage.close();

        FXMLLoader applicationSignIn = new FXMLLoader();
        applicationSignIn.setLocation(getClass().getResource("/views/start.fxml"));
        AnchorPane applicationLayout = applicationSignIn.load();

        Stage primaryStage = new Stage();
        Scene scene = new Scene(applicationLayout);
        primaryStage.setScene(scene);
        scene.setFill(Color.TRANSPARENT);
        primaryStage.initStyle(StageStyle.TRANSPARENT);

        ControllerAccount singInController = applicationSignIn.getController();
        singInController.setUp(serviceUser, serviceFriendRequest, serviceMessage, serviceFriendship, serviceEvent, serviceNotification, primaryStage);
        primaryStage.show();
    }

    public void handleOpenFriendProfile(MouseEvent mouseEvent) {
        sendFriendRequest.setText("Send Friend Request");
        mutualFriendsLabel.setText("Mutual friends");
        User selected = (User) listFindFriends.getSelectionModel().getSelectedItem();

        if (selected == null) return;

        friendNameLabelChat.setText(selected.getFirstName() + " " + selected.getLastName());
        Iterable<User> iterableMutualFriends = serviceUser.getMutualFriends(userLoggedIn, selected);
        List<User> mutualFriends = StreamSupport.stream(iterableMutualFriends.spliterator(), false).collect(Collectors.toList());
        if (mutualFriends.isEmpty())
            mutualFriendsLabel.setText("No mutual friends");
        modelMutualFriendsOnFindFriendsProfileList.setAll(mutualFriends);

        if (serviceFriendRequest.verifySentFriendRequest(userLoggedIn, selected)) {
            sendFriendRequest.setText("Sent ->");
        } else if (serviceFriendRequest.verifyReceivedFriendRequest(userLoggedIn, selected)) {
            sendFriendRequest.setText("Respond");
        }

        newFriendProfile.setVisible(true);

    }

    public void handleCloseFriendProfile(MouseEvent mouseEvent) {
        newFriendProfile.setVisible(false);
    }

    public void handleOpenFriendRequestProfile(MouseEvent mouseEvent) {
        mutualFriendsLabel1.setText("Mutual friends");
        User selected = (User) listFriendRequest.getSelectionModel().getSelectedItem();

        if (selected == null) return;

        friendNameTextFieldOnFriendRequestProfilePane.setText(selected.getFirstName() + " " + selected.getLastName());
        Iterable<User> iterableMutualFriends = serviceUser.getMutualFriends(userLoggedIn, selected);
        List<User> mutualFriends = StreamSupport.stream(iterableMutualFriends.spliterator(), false).collect(Collectors.toList());
        if (mutualFriends.isEmpty())
            mutualFriendsLabel1.setText("No mutual friends");
        modelMutualFriendsOnFriendRequestProfilePane.setAll(mutualFriends);

        friendRequestProfile.setVisible(true);
    }

    public void handleCloseFriendRequestProfile(MouseEvent mouseEvent) {
        friendRequestProfile.setVisible(false);
    }

    public void handleRemoveFriend(ActionEvent event) {
        FriendDTO selected = (FriendDTO) listFriends.getSelectionModel().getSelectedItem();
        if (selected != null) {
            Friendship friendship = serviceFriendship.remove(new Tuple<>(selected.getId(), userLoggedIn.getId()));

            paneFriendProfile.setVisible(false);
        }

    }

    public void handleOpenChatWithFriend(ActionEvent mouseEvent) {
        paneFriendProfile.setVisible(false);
        textAreaChat.setAccessibleText("");
        textAreaChat.setText("");

        FriendDTO selected = (FriendDTO) listFriends.getSelectionModel().getSelectedItem();
        if (selected == null)
            return;
        chatHeader.setText(selected.getFirstName() + " " + selected.getLastName());

        sendTo = new ArrayList<>();
        sendTo.add(serviceUser.findOne(selected.getId()));
        setUpChat(false);

        anchorPaneChat.setVisible(true);
        scrollPaneMessages.setVvalue(1.0);


    }

    public void handleSendMessageOnEnter(KeyEvent keyEvent) {
        if (keyEvent.getCode().equals(KeyCode.ENTER))
            handleSendMessage(null);
    }

    public void handleSendMessage(MouseEvent mouseEvent) {
        if (textAreaChat.getText().equals(""))
            return;
        Pane pane = new Pane();
        Text text = new Text(textAreaChat.getText());
        pane.layout();

        double width = text.getLayoutBounds().getWidth();
        double padding = 40;
        TextArea textArea = new TextArea();

        int rowCount = (int) ((width + padding) / 180);
        double rowWidth = width + padding;
        textArea.setPrefRowCount(rowCount);
        textArea.setText(textAreaChat.getText());
        textArea.setWrapText(true);
        textArea.setEditable(false);

        if (rowWidth > 180)
            rowWidth = 180;
        rowCount = (int) (rowWidth / 180);
        textArea.setPrefRowCount(rowCount);

        textArea.setMaxWidth(rowWidth);
        textArea.setLayoutX(210 - rowWidth);

        if (!textAreaChat.getAccessibleText().equals("")) {
            padding = 20;
            text.setText("Replied to: " + textAreaChat.getAccessibleText());
            Label replyLabel = new Label("Replied to: " + textAreaChat.getAccessibleText());
            width = text.getLayoutBounds().getWidth();
            rowWidth = width + padding;
            replyLabel.setWrapText(true);
            if (rowWidth > 180) {
                rowWidth = 180;
            }
            rowCount = (int) (rowWidth / 180 + 1);
            replyLabel.setMaxHeight(25 * rowCount);
            replyLabel.setMaxWidth(rowWidth);
            replyLabel.setLayoutX(210 - rowWidth);
            textArea.setLayoutY(25 * rowCount);
            replyLabel.setAlignment(Pos.BOTTOM_RIGHT);

            pane.getChildren().add(replyLabel);
            serviceMessage.replyUser(userLoggedIn.getId(), isReplyID, textAreaChat.getText(), serviceMessage.findOne(isReplyID).getFrom().getId());
        } else {
            List<Long> idsTo = new ArrayList<>();
            sendTo.forEach(x -> {
                idsTo.add(x.getId());
            });
            serviceMessage.add(userLoggedIn.getId(), idsTo, textAreaChat.getText());
        }

        pane.getChildren().add(textArea);
        vBoxChat.getChildren().add(pane);

        scrollPaneMessages.setVvalue(1.0);

        textAreaChat.setText("");
    }

    private void listTo(Message selected) {
        List<User> to = selected.getTo();
        if (to.size() == 1) {
            sendTo = new ArrayList<>();
            sendTo.add(selected.getFrom());
            chatHeader.setText(selected.getFrom().getFirstName() + " " + selected.getFrom().getLastName());
            return;
        }
        sendTo = new ArrayList<>();
        sendTo.addAll(to);
        sendTo.add(selected.getFrom());
        sendTo.remove(userLoggedIn);

        StringBuilder s = new StringBuilder();
        for (User user : to) {
            if (s.toString().equals(""))
                s.append(user.getFirstName()).append(" ").append(user.getLastName());
            else
                s.append(", ").append(user.getFirstName()).append(" ").append(user.getLastName());
        }
        chatHeader.setText(s.toString());

    }

    public void handleOpenMessageInChat(MouseEvent mouseEvent) {
        Message selected = tableMessages.getSelectionModel().getSelectedItem();
        textAreaChat.setAccessibleText("");
        textAreaChat.setText("");
        if (selected == null)
            return;

        anchorPaneChat.setVisible(true);

        listTo(selected);
        scrollPaneMessages.setVvalue(1.0);

        setUpChat(true);
    }

    public void replyOneUser(ActionEvent event) {
        Message selected = tableMessages.getSelectionModel().getSelectedItem();
        if (selected == null || selected.getFrom().equals(userLoggedIn))
            return;

        sendTo = new ArrayList<>();
        sendTo.add(selected.getFrom());

        chatHeader.setText(selected.getFrom().getFirstName() + " " + selected.getFrom().getLastName());
        scrollPaneMessages.setVvalue(1.0);

        setUpChat(false);
    }

    public void handleOpenGroupList(ActionEvent event) {
        anchorPaneFriendsList.setVisible(false);
        anchorPaneGroupChat.setVisible(false);
        anchorPaneGroupChatList.setVisible(false);
        anchorPaneFriendsList.setVisible(false);
        labelTextFieldGroupOrFriendsList.setText("");
        paneFriendProfile.setVisible(false);
        anchorPaneGroupMessages.setVisible(true);

    }

    public void handleOpenFriendsList(ActionEvent event) {
        anchorPaneGroupChat.setVisible(false);
        anchorPaneGroupChatList.setVisible(false);
        anchorPaneGroupMessages.setVisible(false);
        paneFriendProfile.setVisible(false);
        anchorPaneFriendsList.setVisible(true);
        labelTextFieldGroupOrFriendsList.setText("Friends List");
    }

    public void handleExitOpenedFriendProfile(MouseEvent mouseEvent) {
        paneFriendProfile.setVisible(false);

    }

    public void handleOpenProfile(MouseEvent mouseEvent) {
        FriendDTO selected = (FriendDTO) listFriends.getSelectionModel().getSelectedItem();
        if (selected == null)
            return;

        friendNameTextField.setText(selected.getFirstName() + " " + selected.getLastName());
        mutualFriendsLabelFromFriendListProfile.setText("Mutual Friends");

        Iterable<User> iterableMutualFriends = serviceUser.getMutualFriends(userLoggedIn, serviceUser.findOne(selected.getId()));
        List<User> mutualFriends = StreamSupport.stream(iterableMutualFriends.spliterator(), false).collect(Collectors.toList());
        if (mutualFriends.isEmpty())
            mutualFriendsLabelFromFriendListProfile.setText("No mutual friends");
        modelMutualFriendsOnFriendProfilePane.setAll(mutualFriends);
        paneFriendProfile.setVisible(true);

    }

    public void handleOpenCreatorGroupChat(ActionEvent event) {
        anchorPaneFriendsList.setVisible(false);
        paneFriendProfile.setVisible(false);
        anchorPaneGroupMessages.setVisible(false);
        labelTextFieldGroupOrFriendsList.setText("Choose participants");
        anchorPaneChat.setVisible(false);
        anchorPaneGroupChat.setVisible(true);
        anchorPaneGroupChatList.setVisible(true);

    }

    public void handleCreateGroupChat(ActionEvent event) {

        if (friendsChosenForGroupChat.size() == 0)
            return;

        sendTo = new ArrayList<>();
        friendsChosenForGroupChat.forEach(x -> sendTo.add(serviceUser.findOne(x.getId())));

        anchorPaneGroupChat.setVisible(false);
        anchorPaneGroupChatList.setVisible(false);
        labelTextFieldGroupOrFriendsList.setText("");
        anchorPaneChat.setVisible(true);
        anchorPaneGroupMessages.setVisible(true);

        StringBuilder chatHeaderNames = new StringBuilder();
        for (User user : sendTo) {
            if (chatHeaderNames.toString().equals(""))
                chatHeaderNames.append(user.getFirstName()).append(" ").append(user.getLastName());
            else
                chatHeaderNames.append(", ").append(user.getFirstName()).append(" ").append(user.getLastName());
        }
        chatHeader.setText(chatHeaderNames.toString());
        setUpChat(true);

    }

    public void deletePersonGroupChat(MouseEvent mouseEvent) {
        FriendDTO selected = (FriendDTO) listChosenGroupChat.getSelectionModel().getSelectedItem();

        if (selected == null) {
            return;
        }

        friendsChosenForGroupChat.remove(selected);
        modelFriendsChosenForGroupChat.setAll(friendsChosenForGroupChat);
        listChosenGroupChat.setItems(modelFriendsChosenForGroupChat);

    }

    public void handleAddToNewGroupList(MouseEvent mouseEvent) {
        FriendDTO selected = (FriendDTO) listFriendsToChoseForGroupChat.getSelectionModel().getSelectedItem();

        if (selected == null || friendsChosenForGroupChat.contains(selected))
            return;

        friendsChosenForGroupChat.add(selected);
        modelFriendsChosenForGroupChat.setAll(friendsChosenForGroupChat);
        listChosenGroupChat.setItems(modelFriendsChosenForGroupChat);


    }

    public void handleOpenActivityLog(ActionEvent event) {
        vBoxActivityLog.getChildren().removeAll(vBoxActivityLog.getChildren());
        labelFailGenerateActivityLog.setText("");
        friendshipsRadioButton.setSelected(false);
        specificFriendRadioButton.setSelected(false);
        messagesRadioButton.setSelected(false);
        fromDatePickerActivityLog.setValue(null);
        toDatePickerActivityLog.setValue(null);
        anchorPaneActivityLog.setVisible(true);
        fileNameTextField.setText("");

    }

    public void handleGeneratePdf(ActionEvent event) {
        LocalDate from = fromDatePickerActivityLog.getValue();
        LocalDate to = toDatePickerActivityLog.getValue();

        if (from == null || to == null) {
            labelFailGenerateActivityLog.setText("Choose the period of time to search for the activity!");
            return;
        }
        if (fileNameTextField.getText().equals("")) {
            labelFailGenerateActivityLog.setText("Choose a File Name where to save your work!");
            return;
        }

        ActivityLog activityLog = new ActivityLog(userLoggedIn, from, to, fileNameTextField.getText());

        if (friendshipsRadioButton.isSelected()) {
            if (!messagesRadioButton.isSelected()) {
                try {
                    List<FriendDTO> friendships = serviceUser.reportAllFriendships(from, to, userLoggedIn);
                    if (friendships == null || friendships.size() == 0) {
                        labelFailGenerateActivityLog.setText("Nothing to generate!");
                        return;
                    }
                    activityLog.createPdf(null, friendships, null);
                    labelFailGenerateActivityLog.setText("PDF successfully create!");
                } catch (IOException ioException) {
                    labelFailGenerateActivityLog.setText("Generate PDF failed. Try again later.");
                }
            } else {
                try {
                    List<Message> messages = serviceMessage.reportAllMessages(from, to, userLoggedIn);
                    List<FriendDTO> friendships = serviceUser.reportAllFriendships(from, to, userLoggedIn);
                    if (messages == null && friendships == null || messages.size() == 0 && friendships.size() == 0) {
                        labelFailGenerateActivityLog.setText("Nothing to generate!");
                        return;
                    }
                    activityLog.createPdf(messages, friendships, null);
                    labelFailGenerateActivityLog.setText("PDF successfully create!");

                } catch (IOException ioException) {
                    labelFailGenerateActivityLog.setText("Generate PDF failed. Try again later.");
                }
            }


        } else if (messagesRadioButton.isSelected()) {
            if (specificFriendRadioButton.isSelected()) {
                FriendDTO user = chooseFriendComboBox.getValue();
                if (user == null) {
                    labelFailGenerateActivityLog.setText("Choose a friend to search for the activity!");
                } else {
                    try {
                        List<Message> messages = serviceMessage.reportAllMessagesFromSpecificFriend(serviceUser.findOne(user.getId()), userLoggedIn, from, to);
                        if (messages.size() == 0) {
                            labelFailGenerateActivityLog.setText("Nothing to generate!");
                            return;
                        }
                        activityLog.createPdf(messages, null, serviceUser.findOne(user.getId()));
                        labelFailGenerateActivityLog.setText("PDF successfully create!");

                    } catch (IOException ioException) {
                        labelFailGenerateActivityLog.setText("Generate PDF failed. Try again later.");
                    }
                }
            } else {
                try {
                    List<Message> messages = serviceMessage.reportAllMessages(from, to, userLoggedIn);
                    if (messages == null) {
                        labelFailGenerateActivityLog.setText("Nothing to generate!");
                        return;
                    }
                    activityLog.createPdf(messages, null, null);
                    labelFailGenerateActivityLog.setText("PDF successfully create!");

                } catch (IOException ioException) {
                    labelFailGenerateActivityLog.setText("Generate PDF failed. Try again later.");
                }
            }
        }

    }

    public void handleGenerateActivityLog(ActionEvent event) {
        labelFailGenerateActivityLog.setText("");
        LocalDate from = fromDatePickerActivityLog.getValue();
        LocalDate to = toDatePickerActivityLog.getValue();

        if (from == null || to == null) {
            labelFailGenerateActivityLog.setText("Choose the period of time to search for the activity!");
            return;
        }
        if (friendshipsRadioButton.isSelected()) {
            if (!messagesRadioButton.isSelected()) {
                List<FriendDTO> friendships = serviceUser.reportAllFriendships(from, to, userLoggedIn);
                setUpActivityLog(null, friendships);
            } else {
                List<Message> messages = serviceMessage.reportAllMessages(from, to, userLoggedIn);
                List<FriendDTO> friendships = serviceUser.reportAllFriendships(from, to, userLoggedIn);
                setUpActivityLog(messages, friendships);
            }


        } else if (messagesRadioButton.isSelected()) {
            if (specificFriendRadioButton.isSelected()) {
                FriendDTO user = chooseFriendComboBox.getValue();
                if (user == null) {
                    labelFailGenerateActivityLog.setText("Choose a friend to search for the activity!");
                } else {
                    List<Message> messages = serviceMessage.reportAllMessagesFromSpecificFriend(serviceUser.findOne(user.getId()), userLoggedIn, from, to);
                    setUpActivityLog(messages, null);
                }
            } else {
                List<Message> messages = serviceMessage.reportAllMessages(from, to, userLoggedIn);
                setUpActivityLog(messages, null);
            }
        }


    }

    private void setUpActivityLog(List<Message> messages, List<FriendDTO> friendships) {
        vBoxActivityLog.getChildren().removeAll(vBoxActivityLog.getChildren());
        if (friendships == null) {
            messages.forEach(message -> {
                Pane pane = new Pane();
                String string = "Message from: " + message.getFrom() + "\n" + message.getMessage();
                TextArea textArea = new TextArea(string);
                textArea.setPrefWidth(380);
                textArea.setPrefHeight(50);
                textArea.setMinHeight(25);
                textArea.setLayoutX(10);
                textArea.setWrapText(true);
                pane.getChildren().add(textArea);
                vBoxActivityLog.getChildren().add(pane);
            });
        } else if (messages == null) {
            friendships.forEach(friendship -> {
                Pane pane = new Pane();
                TextArea textArea = new TextArea();
                textArea.setText("You and " + friendship.getFirstName() + " " + friendship.getLastName() + " became friends on " + friendship.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
                textArea.setPrefWidth(380);
                textArea.setPrefHeight(20);
                textArea.setMinHeight(25);
                textArea.setLayoutX(75);
                textArea.setWrapText(true);
                pane.getChildren().add(textArea);
                vBoxActivityLog.getChildren().add(pane);
            });
        } else {
            mergeFriendshipsMessages(messages, friendships);
        }
    }

    private void mergeFriendshipsMessages(List<Message> messages, List<FriendDTO> friendships) {

        int messageIndex = 0, friendshipIndex = 0;
        while (messageIndex < messages.size() && friendshipIndex < friendships.size()) {
            Pane pane = new Pane();
            TextArea textArea = new TextArea();
            if (messages.get(messageIndex).getDate().isBefore(friendships.get(friendshipIndex).getDate())) {
                String string = "Message from: " + messages.get(messageIndex).getFrom() + "\n" + messages.get(messageIndex).getMessage();
                textArea.setText(string);
                textArea.setLayoutX(10);
                textArea.setPrefHeight(50);
                messageIndex += 1;
            } else {

                textArea.setText("You and " + friendships.get(friendshipIndex).getFirstName() + " " + friendships.get(friendshipIndex).getLastName() + " became friends on " + friendships.get(friendshipIndex).getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
                textArea.setLayoutX(75);
                textArea.setPrefHeight(20);
                friendshipIndex += 1;
            }
            textArea.setWrapText(true);
            textArea.setPrefWidth(380);
            textArea.setMinHeight(25);

            pane.getChildren().add(textArea);
            vBoxActivityLog.getChildren().add(pane);

        }

        while (messageIndex < messages.size()) {
            Message message = messages.get(messageIndex);
            Pane pane = new Pane();
            String string = "Message from: " + message.getFrom() + "\n" + message.getMessage();
            TextArea textArea = new TextArea(string);
            textArea.setPrefWidth(380);
            textArea.setPrefHeight(50);
            textArea.setMinHeight(25);
            textArea.setLayoutX(10);
            textArea.setWrapText(true);
            pane.getChildren().add(textArea);
            vBoxActivityLog.getChildren().add(pane);
            messageIndex += 1;
        }

        while (friendshipIndex < friendships.size()) {
            FriendDTO friendship = friendships.get(friendshipIndex);
            Pane pane = new Pane();
            TextArea textArea = new TextArea();
            textArea.setText("You and " + friendship.getFirstName() + " " + friendship.getLastName() + " became friends on " + friendship.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")));
            textArea.setPrefWidth(380);
            textArea.setPrefHeight(20);
            textArea.setMinHeight(25);
            textArea.setLayoutX(75);
            textArea.setWrapText(true);
            pane.getChildren().add(textArea);
            vBoxActivityLog.getChildren().add(pane);
            friendshipIndex += 1;
        }
    }

    public void handleCloseActivityLogAnchorPane(ActionEvent event) {
        anchorPaneActivityLog.setVisible(false);
    }

    public void handleMakeAvailableComboBox(MouseEvent mouseEvent) {
        chooseFriendComboBox.setDisable(false);
        friendshipsRadioButton.setSelected(false);
    }

    public void handleSelectedFriendshipsRadioButton(MouseEvent mouseEvent) {
        specificFriendRadioButton.setSelected(false);
        chooseFriendComboBox.setDisable(true);
    }

    public void handleScrollFriendsList(ScrollEvent scrollEvent) {
        FriendDTO selected = listFriends.getSelectionModel().getSelectedItem();
        if (scrollEvent.getDeltaY() > 0) {
            modelFriendsList.setAll(serviceUser.getPreviousFriends(userLoggedIn.getId()));
        } else {
            modelFriendsList.setAll(serviceUser.getNextFriends(userLoggedIn.getId()));
        }

        if (selected != null)
            friend = selected;

        selected = listFriends.getSelectionModel().getSelectedItem();

        if (selected == null) {
            if (modelFriendsList.contains(friend))
                listFriends.getSelectionModel().select(friend);
            else
                listFriends.getSelectionModel().select(null);
        } else {
            friend = selected;
            listFriends.getSelectionModel().select(friend);
        }

    }

    public void handleScrollFriendRequestTable(ScrollEvent scrollEvent) {
        UserFriendRequestDto selected = tableFriendRequest.getSelectionModel().getSelectedItem();

        if (scrollEvent.getDeltaY() > 0) {
            modelFriendRequests.setAll(serviceFriendRequest.getPreviousFriendRequests(userLoggedIn.getId()));
        } else {
            modelFriendRequests.setAll(serviceFriendRequest.getNextFriendRequests(userLoggedIn.getId()));
        }

        if (selected != null)
            userFriendRequestDto = selected;

        selected = tableFriendRequest.getSelectionModel().getSelectedItem();

        if (selected == null) {
            if (modelFriendRequests.contains(userFriendRequestDto))
                tableFriendRequest.getSelectionModel().select(userFriendRequestDto);
            else
                tableFriendRequest.getSelectionModel().select(null);
        } else {
            userFriendRequestDto = selected;
            tableFriendRequest.getSelectionModel().select(userFriendRequestDto);
        }

    }

    public void handleScrollFindFriendsList(ScrollEvent scrollEvent) {

        User selected = listFindFriends.getSelectionModel().getSelectedItem();

        if (scrollEvent.getDeltaY() > 0) {
            modelFindFriends.setAll(serviceUser.getPreviousFindFriends(userLoggedIn.getId()));
        } else {
            modelFindFriends.setAll(serviceUser.getNextFindFriends(userLoggedIn.getId()));
        }

        if (selected != null)
            findFriend = selected;

        selected = listFindFriends.getSelectionModel().getSelectedItem();

        if (selected == null) {
            if (modelFindFriends.contains(findFriend))
                listFindFriends.getSelectionModel().select(findFriend);
            else
                listFindFriends.getSelectionModel().select(null);
        } else {
            findFriend = selected;
            listFindFriends.getSelectionModel().select(findFriend);
        }

    }

    public void handleScrollSentFriendRequestsList(ScrollEvent scrollEvent) {
        User selected = listFriendRequest.getSelectionModel().getSelectedItem();
        if (scrollEvent.getDeltaY() > 0) {
            modelSentFriendRequest.setAll(serviceFriendRequest.getPreviousSentFriendRequests(userLoggedIn.getId()));
        } else {
            modelSentFriendRequest.setAll(serviceFriendRequest.getNextSentFriendRequests(userLoggedIn.getId()));
        }

        if (selected != null)
            sentriendRequest = selected;

        selected = listFriendRequest.getSelectionModel().getSelectedItem();

        if (selected == null) {
            if (modelSentFriendRequest.contains(sentriendRequest))
                listFriendRequest.getSelectionModel().select(sentriendRequest);
            else
                listFriendRequest.getSelectionModel().select(null);
        } else {
            sentriendRequest = selected;
            listFriendRequest.getSelectionModel().select(sentriendRequest);
        }
    }

    public void handleScrollEventList(ScrollEvent scrollEvent) {
        IncomingEvent selected = listEvents.getSelectionModel().getSelectedItem();
        if (scrollEvent.getDeltaY() > 0) {
            modelIncomingEvents.setAll(serviceEvent.getPreviousEvents());
        } else {
            modelIncomingEvents.setAll(serviceEvent.getNextEvents());
        }

        if (selected != null)
            scrolledEvent = selected;

        selected = listEvents.getSelectionModel().getSelectedItem();

        if (selected == null) {
            if (modelIncomingEvents.contains(scrolledEvent))
                listEvents.getSelectionModel().select(scrolledEvent);
            else
                listEvents.getSelectionModel().select(null);
        } else {
            scrolledEvent = selected;
            listEvents.getSelectionModel().select(scrolledEvent);
        }
    }


    public void handleSaveNewEventCreated(ActionEvent event) {
        String title = titleTextField.getText();
        if (title.equals("")) {
            errorCreateEventLabel.setText("Please write a title!");
            return;
        }
        String location = locationTextField.getText();
        if (location.equals("")) {
            errorCreateEventLabel.setText("Please enter a location!");
            return;
        }
        String description = descriptionTextArea.getText();
        if (description.equals("")) {
            errorCreateEventLabel.setText("Please conceive a description!");
            return;
        }
        LocalDate date = eventDatePicker.getValue();
        if (date == null) {
            errorCreateEventLabel.setText("All events have a date!");
            return;
        }
        String time = hourTextField.getText();
        if (!time.matches("^((?:[01]\\d|2[0-3]):[0-5]\\d$)")) {
            errorCreateEventLabel.setText("Please choose an existing hour!");
            return;
        }
        LocalDateTime localDateTime = LocalDateTime.parse(date.toString() + " " + time, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        errorCreateEventLabel.setText("");
        serviceEvent.addEvent(title, description, localDateTime, location, userLoggedIn);
        handleCreateNewEvent(null);
        int indexNewEvent = modelIncomingEvents.size() - 1;
        listEvents.scrollTo(indexNewEvent);
        listEvents.getSelectionModel().select(indexNewEvent);
        handleOpenDetailsAboutEvent(null);
    }

    public void handleCreateNewEvent(ActionEvent event) {
        subscribeBtn.setText("Subscribe");
        remindMeBtn.setText("Remind me");
        subscribeBtn.setDisable(true);
        remindMeBtn.setDisable(true);
        hourTextField.setText("");
        eventDatePicker.setValue(null);
        titleTextField.setText("");
        descriptionTextArea.setText("");
        locationTextField.setText("");
        modelAttendingUsers.clear();
    }

    public void handleOpenEventAnchorPane(ActionEvent event) {
        handleCreateNewEvent(null);
        eventsAnchorPane.setVisible(true);
        listEvents.scrollTo(0);
        listEvents.getSelectionModel().select(0);
    }

    public void handleBackHyperlinkEventAnchorPane(ActionEvent event) {
        eventsAnchorPane.setVisible(false);
        handleCreateNewEvent(null);
    }

    public void handleOpenDetailsAboutEvent(MouseEvent mouseEvent) {
        IncomingEvent event = (IncomingEvent) listEvents.getSelectionModel().getSelectedItem();
        if (event == null)
            return;

        remindMeBtn.setDisable(false);
        subscribeBtn.setDisable(false);

        if (serviceEvent.checkSubscription(event, userLoggedIn)) {
            subscribeBtn.setText("Unsubscribe");
            remindMeBtn.setDisable(false);
            if (serviceEvent.checkGettingNotification(event, userLoggedIn)) {
                remindMeBtn.setText("Don't remind me");
            } else {
                remindMeBtn.setText("Remind me");
            }
        } else {
            subscribeBtn.setText("Subscribe");
            remindMeBtn.setText("Remind me");
            remindMeBtn.setDisable(true);
        }

        titleTextField.setText(event.getTitle());
        locationTextField.setText(event.getLocation());
        descriptionTextArea.setText(event.getDescription());
        errorCreateEventLabel.setText("");
        eventDatePicker.setValue(event.getDate().toLocalDate());
        hourTextField.setText(event.getDate().getHour() + " : " + event.getDate().getMinute());
        eventDatePicker.setEditable(false);
        modelAttendingUsers.setAll(serviceEvent.getAllAttendantsForAnEvent(event));
    }

    public void handleSubscribeButton(ActionEvent event) {
        IncomingEvent incomingEvent = (IncomingEvent) listEvents.getSelectionModel().getSelectedItem();
        if (incomingEvent == null) {
            incomingEvent = scrolledEvent;
            if (incomingEvent == null)
                return;
        }
        int indexLastEventSelected = listEvents.getSelectionModel().getSelectedIndex();


        if (serviceEvent.checkSubscription(incomingEvent, userLoggedIn)) {
            serviceEvent.unsubscribe(incomingEvent, userLoggedIn);
            subscribeBtn.setText("Subscribe");
            remindMeBtn.setText("Remind me");
            remindMeBtn.setDisable(true);
        } else {
            serviceEvent.subscribe(incomingEvent, userLoggedIn);
            subscribeBtn.setText("Unsubscribe");
            remindMeBtn.setDisable(false);
            remindMeBtn.setText("Don't remind me");
        }
        listEvents.scrollTo(indexLastEventSelected);
        listEvents.getSelectionModel().select(indexLastEventSelected);
        List<User> allParticipants = serviceEvent.getAllAttendantsForAnEvent(incomingEvent);
        modelAttendingUsers.setAll(allParticipants);

    }

    public void handleRemindMeButton(ActionEvent event) {
        IncomingEvent incomingEvent = (IncomingEvent) listEvents.getSelectionModel().getSelectedItem();
        if (incomingEvent == null)
            return;

        int indexLastEventSelected = listEvents.getSelectionModel().getSelectedIndex();
        ;

        if (serviceEvent.checkGettingNotification(incomingEvent, userLoggedIn)) {
            serviceEvent.stopGettingNotifications(incomingEvent, userLoggedIn);
            remindMeBtn.setText("Remind me");
        } else {
            serviceEvent.getNotifications(incomingEvent, userLoggedIn);
            remindMeBtn.setText("Don't remind me");
        }
        listEvents.scrollTo(indexLastEventSelected);
        listEvents.getSelectionModel().select(indexLastEventSelected);
    }


    public void handleOpenNotificationList(ActionEvent event) {
        if (listNotifications.isVisible()) {
            listNotifications.setVisible(false);
            notificationIcon.setStyle("-fx-image: url('/images/notificationWhite.png')");

        } else {
            listNotifications.setVisible(true);
            notificationIcon.setStyle("-fx-image: url('/images/notificationWhite.png')");
            serviceNotification.updateAllNotifications(userLoggedIn);
        }
    }

    public void handleOpenNotificationFromIcon(MouseEvent mouseEvent) {
        handleOpenNotificationList(null);
    }

    public void handleOpenSpecificEvent(MouseEvent mouseEvent) {
        Notification notification = (Notification) listNotifications.getSelectionModel().getSelectedItem();
        handleOpenEventAnchorPane(null);
        listEvents.getSelectionModel().select(serviceEvent.findOne(notification.getIdEvent()));
        int indexLastEventSelected = listEvents.getSelectionModel().getSelectedIndex();
        listEvents.scrollTo(indexLastEventSelected);
        handleOpenDetailsAboutEvent(null);

    }


}