package socialnetwork.domain;

public class Participant extends Entity<Tuple<Long, Long>> {
    private Long idUser;
    private Long idEvent;
    private Boolean notification;

    public Participant(Long idUser, Long idEvent, Boolean notification) {
        this.idUser = idUser;
        this.idEvent = idEvent;
        this.notification = notification;

        setId(new Tuple<>(idUser, idEvent));
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public Long getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(Long idEvent) {
        this.idEvent = idEvent;
    }

    public Boolean getNotification() {
        return notification;
    }

    public void setNotification(Boolean notification) {
        this.notification = notification;
    }
}
