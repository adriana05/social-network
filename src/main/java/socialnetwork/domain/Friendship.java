package socialnetwork.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;


public class Friendship extends Entity<Tuple<Long, Long>> {

    LocalDateTime date;

    public Friendship(Tuple<Long, Long> id, LocalDateTime date) {
        setId(id);
        setDate(date);
    }

    public Friendship(Tuple<Long, Long> id) {
        setId(id);
        date = LocalDateTime.now();
    }

    @Override
    public String toString() {
        return "Friendship{" +
                "Id-ul primului prieten=" + getId().getLeft() +
                " Id-ul celui de-l doilea prieten=" + getId().getRight() +
                " date=" + date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Friendship)) return false;
        Friendship friendship = (Friendship) o;
        if (getId().getRight().equals(friendship.getId().getRight()) && getId().getLeft().equals(friendship.getId().getLeft()))
            return true;
        return getId().getLeft().equals(friendship.getId().getRight()) && getId().getRight().equals(friendship.getId().getLeft());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getDate());
    }

    /**
     * @return the date when the friendship was created
     */
    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
