package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.Objects;

public class FriendDTO extends Entity<Long> {
    private String firstName;
    private String lastName;
    private LocalDateTime date;

    public FriendDTO(Long id, String firstName, String lastName, LocalDateTime date) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.date = date;
        setId(id);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FriendDTO)) return false;
        FriendDTO friendDto = (FriendDTO) o;
        return getFirstName().equals(friendDto.getFirstName()) &&
                getLastName().equals(friendDto.getLastName()) &&
                getDate().equals(friendDto.getDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getDate());
    }


    @Override
    public String toString() {
        return firstName + ' ' +
                lastName;
    }

}
