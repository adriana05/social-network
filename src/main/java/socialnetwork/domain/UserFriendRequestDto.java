package socialnetwork.domain;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class UserFriendRequestDto extends Entity<Tuple<Long, Long>> {
    private String firstName;
    private String lastName;
    private String status;
    private String dateString;
    private String fullName;

    public UserFriendRequestDto(Tuple<Long, Long> id, String firstName, String lastName, String status, LocalDateTime dateString) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.status = status;
        this.dateString = dateString.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
        fullName = this.firstName + " " + this.lastName;
        setId(id);
    }

    public String getFullName() {
        return fullName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserFriendRequestDto)) return false;
        UserFriendRequestDto that = (UserFriendRequestDto) o;
        return getFirstName().equals(that.getFirstName()) &&
                getLastName().equals(that.getLastName()) &&
                getStatus().equals(that.getStatus()) &&
                getDateString().equals(that.getDateString()) &&
                getFullName().equals(that.getFullName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstName(), getLastName(), getStatus(), getDateString(), getFullName());
    }

    public String getStatus() {
        return status;
    }

    public String getDateString() {
        return dateString;
    }

}
