package socialnetwork.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

public class IncomingEvent extends Entity<Long> {
    private String title;
    private String description;
    private String location;
    private List<User> going;
    private LocalDateTime date;

    public IncomingEvent(String title, String description, List<User> going, LocalDateTime date, String location) {
        this.title = title;
        this.description = description;
        this.going = going;
        this.date = date;
        this.location = location;
    }

    @Override
    public String toString() {
        return title + '\n' + date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm")) +
                " " + location + "\n" + description +
                "\nGoing:" + going;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<User> getGoing() {
        return going;
    }

    public void setGoing(List<User> going) {
        this.going = going;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof IncomingEvent)) return false;
        IncomingEvent that = (IncomingEvent) o;
        return getTitle().equals(that.getTitle()) &&
                getDescription().equals(that.getDescription()) &&
                getLocation().equals(that.getLocation()) &&
                getGoing().equals(that.getGoing()) &&
                getDate().equals(that.getDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getTitle(), getDescription(), getLocation(), getGoing(), getDate());
    }
}
