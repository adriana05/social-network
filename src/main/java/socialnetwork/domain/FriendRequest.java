package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.Objects;

public class FriendRequest extends Entity<Tuple<Long, Long>> {
    private String status;
    private LocalDateTime date;

    public FriendRequest(Tuple<Long, Long> id, String status, LocalDateTime date) {
        this.status = status;
        setId(id);
        this.date = date;
    }

    public FriendRequest(Tuple<Long, Long> id, String status) {
        this.status = status;
        setId(id);
        date = LocalDateTime.now();
    }

    public LocalDateTime getDate() {
        return date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "ID friend who requests: " + getId().getLeft() + ", ID friend to respond: " +
                getId().getRight() +
                ", status='" + status + '\'';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof FriendRequest)) return false;
        FriendRequest that = (FriendRequest) o;
        return getStatus().equals(that.getStatus()) &&
                getDate().equals(that.getDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStatus(), getDate());
    }
}
