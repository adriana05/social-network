package socialnetwork.domain.validators;

import socialnetwork.domain.Friendship;

public class ValidatorFriendship implements Validator<Friendship> {

    @Override
    public void validate(Friendship entity) throws ValidationException {

        String message_error = "";


        if (entity.getId().getRight().equals(entity.getId().getLeft())) {
            message_error += "Same id(s)!\n";
        }

        if (!message_error.equals("")) {
            throw new ValidationException(message_error);

        }
    }
}
