package socialnetwork.domain.validators;

import socialnetwork.domain.FriendRequest;

public class ValidatorFriendRequest implements Validator<FriendRequest> {


    @Override
    public void validate(FriendRequest friendRequest) {

        String messageError = "";

        if (friendRequest.getId().getRight().equals(friendRequest.getId().getLeft())) {
            messageError += "Cannot send a friend request to the same person!\n";
        }

        if (!messageError.equals("")) {
            throw new ValidationException(messageError);
        }
    }

}
