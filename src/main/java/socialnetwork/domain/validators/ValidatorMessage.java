package socialnetwork.domain.validators;

import socialnetwork.domain.Message;


public class ValidatorMessage implements Validator<Message> {

    @Override
    public void validate(Message message) throws ValidationException {

        String messageError = "";

        if (message.getMessage().equals("")) {
            messageError += "Message must not be null!\n";
        }

        if (message.getTo().contains(message.getFrom())) {
            messageError += "You cannot send a message to yourself!\n";
        }

        if (!messageError.equals("")) {
            throw new ValidationException(messageError);
        }
    }
}
