package socialnetwork.domain.validators;

import socialnetwork.domain.User;

public class ValidatorUser implements Validator<User> {
    @Override
    public void validate(User entity) throws ValidationException {

        String message_error = "";
        if (entity.getFirstName().equals("")) {
            message_error += "Missing First name!\n";
        } else if (!Character.isUpperCase(entity.getFirstName().charAt(0))) {
            message_error += "The First Name is supposed to start \nwith upper case!\n";
        }


        for (int i = 1; i < entity.getFirstName().length(); ++i) {
            if (Character.isUpperCase(entity.getFirstName().charAt(i))) {
                message_error += "Invalid First Name, an upper case \nmight have appeared on an inconvenient position!\n";
                break;
            }
        }

        if (entity.getLastName().equals("")) {
            message_error += "Missing Last name!\n";
        } else if (!Character.isUpperCase(entity.getLastName().charAt(0))) {
            message_error += "The Last Name is supposed to start with upper case!\n";
        }


        for (int i = 1; i < entity.getLastName().length(); ++i) {
            if (Character.isUpperCase(entity.getLastName().charAt(i))) {
                message_error += "Invalid Last Name, an upper case might \nhave appeared on an inconvenient position!\n";
                break;
            }
        }

        if (!entity.getEmail().matches("^[a-z0-9]+@[a-z]+\\.[a-z]+$")) {
            message_error += "Invalid email address!\n";
        }

        if (!entity.getPassword().matches("^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$")) {
            message_error += "Password must contain at least 8 characters and\n at least one upper case and digit\n";
        }


        if (!message_error.equals(""))
            throw new ValidationException(message_error);
    }
}
