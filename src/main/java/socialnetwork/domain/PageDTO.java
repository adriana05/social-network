package socialnetwork.domain;

import java.util.List;

public class PageDTO {
    private String firstName;
    private String lastName;
    private List<FriendDTO> friendsList;
    private Iterable<UserFriendRequestDto> friendRequestsList;
    private Iterable<User> unknownUsersList;
    private Iterable<User> friendRequestsSentList;
    private List<Message> messageList;
    private List<IncomingEvent> eventList;


    public PageDTO(String firstName, String lastName, List<FriendDTO> friendsList, Iterable<UserFriendRequestDto> friendRequestsList, Iterable<User> unknownUsersList, Iterable<User> friendRequestsSentList, List<Message> messageList, List<IncomingEvent> eventList) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.friendsList = friendsList;
        this.friendRequestsList = friendRequestsList;
        this.unknownUsersList = unknownUsersList;
        this.friendRequestsSentList = friendRequestsSentList;
        this.messageList = messageList;
        this.eventList = eventList;
    }

    public List<IncomingEvent> getEventList() {
        return eventList;
    }

    public void setEventList(List<IncomingEvent> eventList) {
        this.eventList = eventList;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<FriendDTO> getFriendsList() {
        return friendsList;
    }

    public void setFriendsList(List<FriendDTO> friendsList) {
        this.friendsList = friendsList;
    }

    public Iterable<UserFriendRequestDto> getFriendRequestsList() {
        return friendRequestsList;
    }

    public void setFriendRequestsList(Iterable<UserFriendRequestDto> friendRequestsList) {
        this.friendRequestsList = friendRequestsList;
    }

    public void setFriendRequestsList(List<UserFriendRequestDto> friendRequestsList) {
        this.friendRequestsList = friendRequestsList;
    }

    public Iterable<User> getUnknownUsersList() {
        return unknownUsersList;
    }

    public void setUnknownUsersList(Iterable<User> unknownUsersList) {
        this.unknownUsersList = unknownUsersList;
    }

    public void setUnknownUsersList(List<User> unknownUsersList) {
        this.unknownUsersList = unknownUsersList;
    }

    public Iterable<User> getFriendRequestsSentList() {
        return friendRequestsSentList;
    }

    public void setFriendRequestsSentList(Iterable<User> friendRequestsSentList) {
        this.friendRequestsSentList = friendRequestsSentList;
    }

    public void setFriendRequestsSentList(List<User> friendRequestsSentList) {
        this.friendRequestsSentList = friendRequestsSentList;
    }

    public List<Message> getMessageList() {
        return messageList;
    }

    public void setMessageList(List<Message> messageList) {
        this.messageList = messageList;
    }
}
