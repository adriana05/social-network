package socialnetwork.domain;


import java.util.List;

public class ReplyMessage extends Message {

    private Message originalMessage;

    public ReplyMessage(User from, List<User> to, String message, Message originalMessage) {
        super(from, to, message);
        this.originalMessage = originalMessage;
        setIsReply(originalMessage.getId());
    }

}
