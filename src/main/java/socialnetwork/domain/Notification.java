package socialnetwork.domain;

import java.time.LocalDateTime;
import java.util.Objects;

public class Notification extends Entity<Long> {

    private Long idUser;
    private String text;
    private boolean isOpened;
    private Long idEvent;
    private LocalDateTime date;

    public Notification(Long idUser, String text, boolean isOpened, Long idEvent, LocalDateTime date) {
        this.idUser = idUser;
        this.text = text;
        this.isOpened = isOpened;
        this.idEvent = idEvent;
        this.date = date;
    }

    @Override
    public String toString() {
        return text;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isOpened() {
        return isOpened;
    }

    public void setOpened(boolean opened) {
        isOpened = opened;
    }

    public Long getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(Long idEvent) {
        this.idEvent = idEvent;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Notification)) return false;
        Notification that = (Notification) o;
        return getText().equals(that.getText());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getText());
    }
}
