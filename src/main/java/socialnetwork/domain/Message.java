package socialnetwork.domain;


import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;

public class Message extends Entity<Long> implements Comparable<Message> {
    private User from;
    private List<User> to;
    private String message;
    private LocalDateTime date;
    private Long isReply;
    private String fullMessage;

    public Message(User from, List<User> to, String message) {
        this.from = from;
        this.to = to;
        this.message = message;
        date = LocalDateTime.now();
        setFullMessage();

        isReply = null;
    }

    private void setFullMessage() {
        fullMessage = "From: " + from.getFirstName() + " " + from.getLastName() + "\nTo: " + toStringList() + "\n" + message;
    }

    public String getFullMessage() {
        return fullMessage;
    }

    public Long getIsReply() {
        return isReply;
    }

    public void setIsReply(Long isReply) {
        this.isReply = isReply;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public List<User> getTo() {
        return to;
    }

    public void setTo(List<User> to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Message)) return false;
        Message message1 = (Message) o;
        return getFrom().equals(message1.getFrom()) &&
                getTo().equals(message1.getTo()) &&
                getMessage().equals(message1.getMessage()) &&
                getDate().equals(message1.getDate());
    }

    public String toStringList() {
        StringBuilder s = new StringBuilder();
        for (User user : to) {
            if (s.toString().equals(""))
                s.append(user.getFirstName()).append(" ").append(user.getLastName());
            else
                s.append(", ").append(user.getFirstName()).append(" ").append(user.getLastName());
        }
        return s.toString();
    }

    @Override
    public String toString() {
        return "Message:\n- ID: " + getId() +
                "\n- from: (" + from.getId() + ") " + from.getFirstName() + " " + from.getLastName() +
                "\n- to: " + toStringList() +
                "\n- message: '" + message + '\'' +
                "\n- date: " + date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm"));
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFrom(), getTo(), getMessage(), getDate());
    }

    @Override
    public int compareTo(Message o) {
        return this.getId().compareTo(o.getId());
    }
}
